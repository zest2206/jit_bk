<?php
require_once 'include/querys.php';
require_once 'include/valdisease.php';
require_once 'include/valprovince.php';
require_once 'include/valdistrict.php';
require_once 'include/valamphures.php';
require_once 'include/valposition.php';
require_once 'include/vallocation.php';
$register = new querys();
$regis = $register->getDataFromTable(
		'tbl_response', array(), array('WHERE Id_Team=?', 'ORDER BY Id_Team ASC'), array($_GET['Id_Team'])
		);
$resive = $register->getDataFromTable(
	'tbl_person', array(), array('WHERE Id_Team=?', 'ORDER BY Id_Team ASC'), array($_GET['Id_Team'])
			 );
$resivelocation = $register->getDataFromTable(
	'tbl_location', array(), array('WHERE Id_Team=?', 'ORDER BY Id_Team ASC'), array($_GET['Id_Team'])
			 );
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<link rel="apple-touch-icon" sizes="76x76" href="Assets/img/apple-icon.png">
	<link rel="icon" type="image/png" sizes="96x96" href="Assets/img/favicon.png">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>Joint investigation team</title>
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
	<meta name="viewport" content="width=device-width" />
    <!--  Social tags      -->
	<meta name="keywords" content="bootstrap dashboard, creative tim, html dashboard, html css dashboard, web dashboard, paper design, bootstrap dashboard, bootstrap, css3 dashboard, bootstrap admin, paper bootstrap dashboard, frontend, responsive bootstrap dashboard">
	<meta name="description" content="Paper Dashboard is a beautiful Bootstrap Admin Panel for your next project.">
    <!-- Schema.org markup for Google+ -->
	<meta itemprop="name" content="Paper Dashboard by Creative Tim">
	<meta itemprop="description" content="Paper Dashboard is a beautiful Bootstrap Admin Panel for your next project.">
	<meta itemprop="image" content="http://s3.amazonaws.com/creativetim_bucket/products/43/original/opt_pd_thumbnail.jpg">
    <!-- Twitter Card data -->
	<meta name="twitter:card" content="summary_large_image">
	<meta name="twitter:site" content="@creativetim">
	<meta name="twitter:title" content="Paper Dashboard PRO by Creative Tim">
	<meta name="twitter:description" content="Paper Dashboard is a beautiful Bootstrap Admin Panel for your next project.">
	<meta name="twitter:creator" content="@creativetim">
	<meta name="twitter:image" content="http://s3.amazonaws.com/creativetim_bucket/products/43/original/opt_pd_thumbnail.jpg">
    <!-- Open Graph data -->
	<meta property="og:title" content="Paper Dashboard by Creative Tim" />
	<meta property="og:type" content="article" />
	<meta property="og:url" content="http://demos.creative-tim.com/paper-dashboard/dashboard.html" />
	<meta property="og:image" content="http://s3.amazonaws.com/creativetim_bucket/products/43/original/opt_pd_thumbnail.jpg"/>
	<meta property="og:description" content="Paper Dashboard is a beautiful Bootstrap Admin Panel for your next project." />
	<meta property="og:site_name" content="Creative Tim" />
	<!-- Bootstrap core CSS     -->
	<link href="Assets/css/bootstrap.min.css" rel="stylesheet" />
	<!-- Animation library for notifications   -->
	<link href="Assets/css/animate.min.css" rel="stylesheet"/>
	<!--  Paper Dashboard core CSS    -->
	<link href="Assets/css/paper-dashboard.css" rel="stylesheet"/>
	<!--  CSS for Demo Purpose, don't include it in your project     -->
	<!-- <link href="Assets/css/demo.css" rel="stylesheet" /> -->
	<!--  Fonts and icons     -->
	<link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
	<link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>
	<link href="Assets/css/themify-icons.css" rel="stylesheet">
	<script src="Assets/jquery/jquery.min.js" type="text/javascript"></script>
	<!-- Paper Dashboard Core javascript and methods for Demo purpose -->
	<script src="Assets/js/paper-dashboard.js"></script>
	<script src="Assets/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="Assets/bootstrap/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
	<script src="Assets/jquery/jquery-ui.min.js" type="text/javascript"></script>
	<script src="Assets/bootstrap/js/validator.js" type="text/javascript"></script>
	<script src="Assets/jquery/jquery.dataTables.min.js" type="text/javascript"></script>
	<script src="//code.jquery.com/jquery-1.12.4.js" type="text/javascript"></script>
	<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js" type="text/javascript"></script>
</head>
<body>
<div class="wrapper">
	<div class="sidebar" data-background-color="white" data-active-color="danger">
    <!--
		Tip 1: you can change the color of the sidebar's background using: data-background-color="white | black"
		Tip 2: you can change the color of the active button using the data-active-color="primary | info | success | warning | danger"
	-->
		<div class="sidebar-wrapper">
			<div class="logo">
					<a href="" class="simple-text">
						JOINT INVESTIGATION TEAM
					</a>
			</div>
			<ul class="nav">
						 <li>
							 <a href="dashboard.php">
								 <i class="ti-panel"></i>
								 <p>Dashboard</p>
							 </a>
						 </li>
						 <li>
							 <a href="teamfrm.php">
								 <i class="ti-user"></i>
								 <p>กรอกข้อมูลทีมสอบสวนโรค</p>
							 </a>
						 </li>
				 <li>
							 <a href="iaplst.php">
								 <i class="ti-marker"></i>
								 <p>รายงานสถานการณ์ประจำวัน</p>
							 </a>
						 </li>
						 <li>
							 <a href="http://www.boeeoc.moph.go.th/eventbase/calendar/zone99/">
								 <i class="ti-notepad"></i>
								 <p>ปฏิทินแจ้งข่าวการระบาด</p>
							 </a>
						 </li>
						 <li class="active">
							 <a href="teamlst.php">
								 <i class="ti-view-list-alt"></i>
								 <p>ตารางทีมที่ออกสอบสวน</p>
							 </a>
						 </li>
						 <li>
							 <a href="http://203.157.15.110/boe/software/downloadtab.php#tab5">
								 <i class="ti-clipboard"></i>
								 <p>แบบฟอร์มสอบสวนโรค</p>
							 </a>
						 </li>
						 <li>
							 <a href="maps.php">
								 <i class="ti-map"></i>
								 <p>Maps</p>
							 </a>
						 </li>
				<li class="active-pro">
						 </li>
					 </ul>
		</div>
	</div>
	<div class="main-panel">
		<nav class="navbar navbar-default">
			<div class="container-fluid">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar bar1"></span>
						<span class="icon-bar bar2"></span>
						<span class="icon-bar bar3"></span>
					</button>
						<a class="navbar-brand" href="#">ข้อมูลโรคและสถานที่ออกสอบสวนโรค</a>
				</div>
				<div class="collapse navbar-collapse">
					<ul class="nav navbar-nav navbar-right">
						<li>
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
								<i class="ti-panel"></i>
								<p>Stats</p>
							</a>
						</li>
						<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									<i class="ti-bell"></i>
										<p class="notification">5</p>
										<p>Notifications</p>
										<b class="caret"></b>
								</a>
							<ul class="dropdown-menu">
								<li><a href="#">Notification 1</a></li>
								<li><a href="#">Notification 2</a></li>
								<li><a href="#">Notification 3</a></li>
								<li><a href="#">Notification 4</a></li>
								<li><a href="#">Another notification</a></li>
							</ul>
						</li>
						<li>
							<a href="logout.php">
									<i class="ti-power-off"></i>
								<p>logout</p>
							</a>
						</li>
					</ul>
				</div>
			</div>
		</nav>
		<div class="content">
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12 col-md-12">
						<div class="card">
							<div class="header"></div>
							<div class="content">
		<!-- <form name="register" role="form" action="teamLst.php" method="post" enctype="multipart/form-data"> -->
		<!--	<form name="register" role="form" action="addproductPS.php" method="post" enctype="multipart/form-data"> -->
			<fieldset>
				<legend>รหัสทีมสอบสวน</legend>
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
						<div class="form-group required">
							<label for="title" class="control-label">รหัสทีมสอบสวน :</label>
								<input type="text" name="Id_Team" id="Id_Team" class="form-control" placeholder="รหัสทีมสอบสวน" data-error="" value="<?php echo $regis[0]['Id_Team']; ?>" readonly>
							<div class="help-block with-errors"></div>
						</div>
					</div>
				</div>
			</fieldset>
			<fieldset>
				<legend>ระดับทีมสอบสวน</legend>
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
						<div class="form-group required">
							<label for="title"  class="control-label">ระดับทีมสอบสวน :</label>
								<input type="text" name="class_team" id="class_team" class="form-control"  data-error="" value="<?php echo $arr_class_team[$regis[0]['class_team']]; ?>" readonly>
							<div class="help-block with-errors"></div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
							<label for="title" class="control-label">ระดับทีมอื่นๆ :</label>
								<input type="text" name="class_team_other" id="class_team_other" class="form-control"  data-error="" value="<?php echo $regis[0]['class_team_other']; ?>" readonly>
						<div class="help-block with-errors"></div>
					</div>
				</fieldset>
				<fieldset>
					<legend>สถานที่ออกปฏิบัติงาน</legend>
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
							<div class="form-group required">
								<label for="title" class="control-label">จังหวัด :</label>
									<input type="text" name="province_name" id="province_name" class="form-control" placeholder="" value="<?php echo $province[$regis[0]['province_name']]; ?>"readonly>
							</div>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
							<div class="form-group required">
								<label for="title" class="control-label">อำเภอ/เขต :</label>
									<input type="text" name="amphur_name" id="amphur_name" class="form-control" placeholder="" value="<?php echo $amphures[$regis[0]['amphur_name']]; ?>"readonly>
							</div>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
							<div class="form-group required">
								<label for="title" class="control-label">ตำบล/แขวง :</label>
									<input type="text" name="district_name" id="district_name" class="form-control" placeholder="" value="<?php echo $arr_district[$regis[0]['district_name']]; ?>"readonly>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
							<div class="form-group required">
								<label for="title" class="control-label">Latitude :</label>
									<input type="text" name="lat" id="lat" class="form-control" placeholder="" value="<?php echo $resivelocation[0]['lat']; ?>"readonly>
							</div>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
							<div class="form-group required">
								<label for="title" class="control-label">Longitude :</label>
									<input type="text" name="lon" id="lon" class="form-control" placeholder="" value="<?php echo $resivelocation[0]['lng']; ?>"readonly>
								<div class="help-block with-errors"></div>
							</div>
						</div>
					</div>
				</fieldset>
				<fieldset>
					<legend>กลุ่มโรคและภัยที่ออกปฏิบัติงาน</legend>
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
							<div class="form-group required">
								<label for="title" class="control-label">กลุ่มโรค :</label>
									<input type="text" name="groupdisease_name" id="groupdisease_name" class="form-control" placeholder="" value="<?php echo $groupdisease[$regis[0]['groupdisease_name']]; ?>"readonly>
							</div>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
							<div class="form-group required">
								<label for="title" class="control-label">กลุ่มโรคอื่นๆ :</label>
									<input type="text" name="groupdisease_other_name" id="groupdisease_other_name" class="form-control" placeholder="" value="<?php echo $regis[0]['groupdisease_other_name']; ?>"readonly>
								<div class="help-block with-errors"></div>
							</div>
						</div>
					</div>
				</fieldset>
				<fieldset>
					<legend>โรคและภัยที่ออกปฏิบัติงาน</legend>
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
							<div class="form-group required">
								<label for="title" class="control-label">โรค :</label>
									<input type="text" name="disease_name" id="disease_name" class="form-control" placeholder="" value="<?php echo $disease[$regis[0]['disease_name']]; ?>"readonly>
							</div>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
							<div class="form-group required">
								<label for="title" class="control-label">โรคอื่นๆ :</label>
									<input type="text" name="disease_other_name" id="disease_other_name" class="form-control" placeholder="" value="<?php echo $regis[0]['disease_other_name']; ?>"readonly>
								<div class="help-block with-errors"></div>
							</div>
						</div>
					</div>
				</fieldset>
					<!-- Large modal -->
				<fieldset>
					<legend>รายชื่อทีมสอบสวนโรค</legend>
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
								<div class="form-group required">
									<div class="modal-content">
										<div class="container-fluid content">
											<div class="panel panel-info">
												<div class="panel-body">
													<table id="example" class="hover" cellspacing="0" width="100%">
														<thead>
															<tr>
																<th>Id_Team</th>
																<th>ชื่อ</th>
																<th>นามสกุล</th>
																<th>หน่วยงาน</th>
																<th>ตำแหน่ง</th>
															</tr>
														</thead>
														<tfoot>
														</tfoot>
														<tbody>
															<?php
																foreach ($resive as $k => $v) {
																	echo "<tr>\n";
																			echo "<td>".$v['Id_Team']."</td>\n";
																			echo "<td>".$v['firstname']."</td>\n";
																			echo "<td>".$v['surname']."</td>\n";
																			echo "<td>".$arr_division[$v['division']]."</td>\n";
																			echo "<td>".$arr_position[$v['position']]."</td>\n";
																	echo "</tr>\n";
																}
															?>
														</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</fieldset>
				<fieldset>
					<legend class="navbar-brand">วันที่ออกปฏิบัติงาน</legend>
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 col-xl-2">
							<div class="form-group required" >
								<label for="title"  class="control-label">สัปดาห์ที่ :</label>
									<input type="text" name="week_travel" id="week_travel" class="form-control" placeholder="yyyy-mm-dd" value="<?php echo $regis[0]['week_travel']; ?>"readonly>
								<span class="input-group-addon">
							</div>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 col-xl-2">
							<div class="form-group required" >
								<label for="title"  class="control-label">วันที่ :</label>
									<input type="text" name="day_travel" id="day_travel" class="form-control" placeholder="yyyy-mm-dd" value="<?php echo $regis[0]['day_travel']; ?>"readonly>
								<span class="input-group-addon">
							</div>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 col-xl-2">
							<div class="form-group required">
								<label for="title"  class="control-label">เดือน :</label>
									<input type="text" name="month_travel" id="month_travel" class="form-control" placeholder="yyyy-mm-dd" value="<?php echo $month[$regis[0]['month_travel']]; ?>"readonly>
								<span class="input-group-addon">
							</div>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 col-xl-2">
							<div class="form-group required">
								<label for="title"  class="control-label">ปี :</label>
									<input type="text" name="year_travel" id="year_travel" class="form-control" placeholder="yyyy-mm-dd" value="<?php echo $regis[0]['year_travel']; ?>"readonly>
								<span class="input-group-addon">
							</div>
						</div>
					</div>
				</fieldset>
				<fieldset>
					<legend class="navbar-brand">วันที่ปฏิบัติงานเสร็จสิ้น</legend>
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 col-xl-2">
							<div class="form-group required" >
								<label for="title"  class="control-label">สัปดาห์ที่ :</label>
									<input type="text" name="week_return" id="week_return" class="form-control" placeholder="yyyy-mm-dd" value="<?php echo $regis[0]['week_return']; ?>"readonly>
 								<span class="input-group-addon">
							</div>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 col-xl-2">
							<div class="form-group required" >
								<label for="title"  class="control-label">วันที่ :</label>
									<input type="text" name="day_return" id="day_return" class="form-control" placeholder="yyyy-mm-dd" value="<?php echo $regis[0]['day_return']; ?>"readonly>
								<span class="input-group-addon">
							</div>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 col-xl-2">
							<div class="form-group required">
								<label for="title"  class="control-label">เดือน :</label>
									<input type="text" name="month_return" id="month_return" class="form-control" placeholder="yyyy-mm-dd" value="<?php echo $month[$regis[0]['month_return']]; ?>"readonly>
								<span class="input-group-addon">
							</div>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 col-xl-2">
							<div class="form-group required">
								<label for="title"  class="control-label">ปี :</label>
									<input type="text" name="year_return" id="year_return" class="form-control" placeholder="yyyy-mm-dd" value="<?php echo $regis[0]['year_return']; ?>"readonly>
								<span class="input-group-addon">
							</div>
						</div>
					</div>
				</fieldset>
				<fieldset>
					<legend>อื่นๆ</legend>
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
							<div class="form-group required">
								<input rows="5" cols="20" type="text" name="other" id="other" class="form-control" placeholder="อื่นๆ" data-error="" required value="<?php echo $regis[0]['other']; ?>"readonly>
								<div class="help-block with-errors"></div>
							</div>
						</div>
					</div>
				</fieldset>
				<fieldset>
					<button  class="btn btn-danger" onclick="window.location.href='teamLst.php'">ย้อนกลับ</button>
				</fieldset>
			</form>
		</div>
	</div>
</div>
	<script>
	$(document).ready(function() {
		$(document).ready(function() {
				$('#example').DataTable();
		} );
	});
	</script>
	</body>
</html>
