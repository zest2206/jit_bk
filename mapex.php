<?php
	require_once 'include/valdisease.php';
	require_once 'mysql_connect.php';
	require_once 'include/conf.php';
	require_once 'include/querys.php';
	require_once 'include/valprovince.php';
	require_once 'include/valdistrict.php';
	require_once 'include/valamphures.php';
	require_once 'include/valdisease.php';
	require_once 'include/valdate.php';
	require_once 'include/valiap.php';
	require_once 'include/valposition.php';
	require_once 'include/vallocation.php';
		$query = "SELECT
		tbl_response.id,
		tbl_response.Id_Team,
		tbl_response.class_team_other,
		tbl_response.class_team,
		tbl_response.province_name,
		tbl_response.amphur_name,
		tbl_response.district_name,
		tbl_response.groupdisease_name,
		tbl_response.groupdisease_other_name,
		tbl_response.disease_name,
		tbl_response.disease_other_name,
		tbl_response.week_travel,
		tbl_response.day_travel,
		tbl_response.month_travel,
		tbl_response.year_travel,
		tbl_response.week_return,
		tbl_response.day_return,
		tbl_response.month_return,
		tbl_response.year_return,
		tbl_response.other,
		tbl_response.date_entry,
		tbl_location.lat,
		tbl_location.lng,
		tbl_iap.patient_search,
		tbl_iap.collecting1,
		tbl_iap.collecting2,
		tbl_iap.collecting3,
		tbl_iap.collecting4,
		tbl_iap.collecting5,
		tbl_iap.summary_situation1,
		tbl_iap.summary_situation2,
		tbl_iap.summary_situation3,
		tbl_iap.summary_situation4,
		tbl_iap.summary_situation5,
		tbl_iap.disease_control,
		tbl_iap.other

		FROM
		tbl_response
		LEFT JOIN tbl_location ON tbl_response.Id_Team = tbl_location.Id_Team
		LEFT JOIN tbl_iap ON tbl_response.Id_Team = tbl_iap.Id_Team  ;  ";
		// echo $query;
		// exit;
		$locations = $conn->query($query);
		$query = "SELECT disease_name,month_travel,year_travel, COUNT(*) AS NumberOfCount FROM tbl_response GROUP BY disease_name;  ";
		$res = $conn->query($query);


?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<link rel="apple-touch-icon" sizes="76x76" href="Assets/img/apple-icon.png">
	<link rel="icon" type="image/png" sizes="96x96" href="Assets/img/favicon.png">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>Joint investigation team</title>
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
	<meta name="viewport" content="width=device-width" />
	<!-- Bootstrap core CSS     -->
	<link href="Assets/css/bootstrap.min.css" rel="stylesheet" />
	<!-- Animation library for notifications   -->
	<link href="Assets/css/animate.min.css" rel="stylesheet"/>
	<!--  Paper Dashboard core CSS    -->
	<link href="Assets/css/paper-dashboard.css" rel="stylesheet"/>
	<!--  CSS for Demo Purpose, don't include it in your project     -->
	<link href="Assets/css/demo.css" rel="stylesheet" />
	<!--  Fonts and icons     -->
	<link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
	<link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>
	<link href="Assets/css/themify-icons.css" rel="stylesheet">
	<!-- <link rel="stylesheet" href="Assets/css/box.css"> -->
	<script src='https://maps.googleapis.com/maps/api/js?key=AIzaSyCAP4jkz9nPhjRqMskknIor2prtqs8dcK4&v=3.0&sensor=true&language=ee'></script>
</head>
	<body>
		<div class="wrapper">
			<nav class="navbar navbar-default">
				<div class="container-fluid">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar bar1"></span>
							<span class="icon-bar bar2"></span>
							<span class="icon-bar bar3"></span>
						</button>
						<a class="navbar-brand" href="#">ข้อมูลโรคและสถานที่ออกสอบสวนโรค</a>
					</div>
					<div class="collapse navbar-collapse">
						<ul class="nav navbar-nav navbar-right">
							<li>
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
								<i class="ti-panel"></i>
								<p>Stats</p>
								</a>
							</li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
								<i class="ti-bell"></i>
								<p class="notification"></p>
								<p>ดาวน์โหลด</p>
								<b class="caret"></b>
								</a>
						<ul class="dropdown-menu">
								<li><a href="http://203.157.15.110/boe/software/downloadtab.php#tab1">Software</a></li>
								<li><a href="http://203.157.15.110/boe/software/downloadtab.php#tab2">โปรแกรมเฝ้าระวัง</a></li>
								<li><a href="http://203.157.15.110/boe/software/downloadtab.php#tab3">ข้อมูลอื่นๆ</a></li>
								<li><a href="http://203.157.15.110/boe/software/downloadtab.php#tab4">แบบฟอร์ม</a></li>
								<li><a href="http://203.157.15.110/boe/software/downloadtab.php#tab5">แบบฟอร์มสอบสวนโรค</a></li>
						</ul>
							</li>
							<li>
								<a href="logout.php">
								<i class="ti-power-off"></i>
								<p>logout</p>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</nav>
		<div class="content">
			<div class="container-fluid">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
					<div class="card ">
						<div class="header">
							<h4 class="title">แผนที่แสดงการออกสอบสวนโรค</h4>
							<p class="category">แผนที่แสดงการออกสอบสวนโรค</p>
						</div>
					<div class="map">
						<div id="map-canvas" style="width:100%;height:700px"></div>
							<div class="footer"><hr>
								<div class="stats">
									<i class="ti-check"></i> Data information certified
								</div>
							</div>
					</div>
					</div>
				</div>
	</div>
</div>
</div>
<div class="fixed-plugin">
	<div class="dropdown show-dropdown">
		<a href="#" data-toggle="dropdown">
			<i class="fa fa-cog fa-2x"> </i>
		</a>
		<ul class="dropdown-menu">
			<li class="header-title">แสดงเฉพาะแผนที่</li>
			<li class="button-container">
				<div class="">
					<a href="teamlst.php" target="_blank" class="btn btn-info btn-block btn-fill">แผนที่</a>
				</div>
			</li>
			<li class="header-title">เลือกแผนที่</li>
			<li class="adjustments-line text-center">
				<div class="">
					<a href="eocdashboard.php"  title="แผนที่ระบุตำแหน่งของทีม JIT" class="badge filter badge-success"></a>
					<a href="testgeo.php"  title="แผนที่อัตราการออกสอบสวนโรค" class="badge filter badge-warning"></a>
				</div>
			</li>
			<li class="header-title">เลือกเดือน</li>
			<li class="adjustments-line text-center">
					<a href="javascript:void(0)" class="switch-trigger active-color" value="มกราคม">
						<select id="type" onchange="filterMarkers(this.value);">
							<option  class="badge filter badge-info" data-color="info" value="">Please select category</option>
							<option class="badge filter badge-info" data-color="info" value="มกราคม">มกราคม</option>
							<option class="badge filter badge-info" data-color="info" value="กุมภาพันธ์">กุมภาพันธ์</option>
							<option class="badge filter badge-info" data-color="info" value="มีนาคม">มีนาคม</option>
							<option class="badge filter badge-info" data-color="info" value="เมษายน ">เมษายน</option>
							<option class="badge filter badge-info" data-color="info" value="พฤษภาคม">พฤษภาคม</option>
							<option class="badge filter badge-info" data-color="info" value="มิถุนายน">มิถุนายน</option>
							<option class="badge filter badge-info" data-color="info" value="กรกฎาคม">กรกฎาคม</option>
							<option class="badge filter badge-info" data-color="info" value="สิงหาคม">สิงหาคม</option>
							<option class="badge filter badge-info" data-color="info" value="กันยายน">กันยายน</option>
							<option class="badge filter badge-info" data-color="info" value="ตุลาคม">ตุลาคม</option>
							<option class="badge filter badge-info" data-color="info" value="พฤศจิกายน">พฤศจิกายน</option>
							<option class="badge filter badge-info" data-color="info" value="ธันวาคม">ธันวาคม</option>
						</select>
					</a>
			</li>
			<li class="header-title">เลือกระดับทีมสอบสวน</li>
			<li class="adjustments-line text-center">
					<a href="javascript:void(0)" class="switch-trigger active-color">
						<select id="type" onchange="filterMarkers4(this.value);">
							<option  class="badge filter badge-info" data-color="info" value="">Please select category</option>
							<option class="badge filter badge-info" data-color="info" value="1">ส่วนกลาง</option>
							<option class="badge filter badge-info" data-color="info" value="2">สคร.</option>
							<option class="badge filter badge-info" data-color="info" value="3">สสจ.</option>
							<option class="badge filter badge-info" data-color="info" value="4">สสอ.</option>
							<option class="badge filter badge-info" data-color="info" value="5">รพสต.</option>
							<option class="badge filter badge-info" data-color="info" value="6">อื่นๆ</option>
						</select>
					</a>
			</li>
			<li class="header-title">เลือกสัปดาห์ที่ออกสอบสวน</li>
			<li class="adjustments-line text-center">
					<a href="javascript:void(0)" class="switch-trigger active-color">
						<select id="type" onchange="filterMarkers2(this.value);">
							<option class="badge filter badge-info" data-color="info" value="">Please select category</option>
								<?php
									for ($w=1; $w<=53; $w++) { ?>
										<option class="badge filter badge-info" data-color="info" value="<?php echo $w ; ?>"><?php echo $w ; ?></option>
								<?php } ?>
						</select>
					</a>
			</li>
			<li class="header-title">เลือกสัปดาห์ที่กลับจากการสอบสวน</li>
			<li class="adjustments-line text-center">
					<a href="javascript:void(0)" class="switch-trigger active-color">
						<select id="type" onchange="filterMarkers3(this.value);">
							<option class="badge filter badge-info" data-color="info" value="">Please select category</option>
								<?php
									for ($w=1; $w<=53; $w++) { ?>
										<option class="badge filter badge-info" data-color="info" value="<?php echo $w ; ?>"><?php echo $w ; ?></option>
								<?php } ?>
						</select>
					</a>
			</li>
			<li class="header-title">แสดงกราฟและแผนที่</li>
			<li class="button-container">
				<div class="">
					<a href="chartboard.php"  class="btn btn-info btn-block btn-fill">กราฟ</a>
				</div>
			</li>
			<li class="header-title">เลือกรูปแบบของกราฟ</li>
			<li class="adjustments-line text-center">
					<a href="javascript:void(0)" class="switch-trigger active-color">
						<span class="badge filter badge-primary" data-color="primary"></span>
						<span class="badge filter badge-info" data-color="info"></span>
						<span class="badge filter badge-success" data-color="success"></span>
						<span class="badge filter badge-warning" data-color="warning"></span>
						<span class="badge filter badge-danger active" data-color="danger"></span>
					</a>
			</li>
			<li class="header-title">ตรวจสอบทีม joint invastigation team</li>
			<li class="button-container">
				<div class="">
					<a href="teamlst.php" target="_blank" class="btn btn-danger btn-block btn-fill">ตรวจสอบทีม</a>
				</div>
			</li>
			<li class="header-title">MENU</li>
			<li class="button-container">
				<button id="" class="btn btn-social  btn-round">หน้าหลัก</button>
				<button id="" class="btn btn-social  btn-round">ออกจากระบบ</button>
			</li>
		</ul>
	</div>
</div>
						 <script>
						 var gmarkers1 = [];
						  var markers1 = [];
						   isIE11 = !!(navigator.userAgent.match(/Trident/) && navigator.userAgent.match(/rv[ :]11/));
	       					var infowindow = new google.maps.InfoWindow({
	           				content: ''
	       						});
						   // Our markers
						   markers1 =    [
						<?php
							$i = 0;
							while ($row=$locations->FETCH_ASSOC()) {
								$i++;
						?>
								[{
							 url: isIE11 ? 'images/markers/markers/a_<?php echo $row['class_team']; ?>.png' : 'images/markers/markers/a_<?php echo $row['class_team']; ?>.png',scaledSize: new google.maps.Size(50, 50)},
							 '<?php echo '<ul class="nav nav-tabs">' ?>
								    <?php echo '<li class="active">' ?><?php echo '<a data-toggle="tab" href="#home">' ?>สถานที่<?php echo '</a>' ?><?php echo '</li>' ?>
								    <?php echo '<li>' ?><?php echo '<a data-toggle="tab" href="#menu1">' ?>ข้อมูลทีมสอบสวน<?php echo '</a>' ?><?php echo '</li>' ?>
								    <?php echo '<li>' ?><?php echo '<a data-toggle="tab" href="#menu2">' ?>สถานการณ์ประจำวัน<?php echo '</a>' ?><?php echo '</li>' ?>
								    <?php echo '<li>' ?><?php echo '<a data-toggle="tab" href="#menu3">' ?>Menu 3<?php echo '</a>' ?><?php echo '</li>' ?>
								  <?php echo '</ul>' ?>
								  <?php echo '<div class="tab-content">' ?>
								    <?php echo '<div id="home" class="tab-pane fade in active">' ?>
								      <?php echo '<h5>' ?>สถานที่<?php echo '</h5>' ?>
								      <?php echo '<p>' ?><?php echo '<h5>' ?>
								 			 ตำบล : <?php echo $arr_district[$row['district_name']]; ?>
								 	 <?php echo "<br>" ?>
								 			 อำเภอ : <?php echo $arr_amphur[$row['amphur_name']]; ?>
								 	 <?php echo "<br>" ?>
								 			 จังหวัด : <?php echo $arr_province[$row['province_name']]; ?><?php echo '</p>' ?>
								    <?php echo '</div>' ?>
									<?php echo '<div id="menu1" class="tab-pane fade">' ?>
								      <?php echo '<h5>' ?>ข้อมูลทีมสอบสวน<?php echo '</h5>' ?>
								      <?php echo '<p>' ?><?php echo $arr_disease[$row['disease_name']]; ?> <?php echo $row['disease_other_name']; ?>
									  <?php echo "<br>" ?>
									  กลุ่มโรค : <?php echo $arr_groupdisease[$row['groupdisease_name']]; ?>
													 <?php echo "<br>" ?>
									 รหัสทีมที่ออกสอบสวนโรค : <?php echo $row['Id_Team']; ?>
									 <?php echo "<br>" ?>
									 ระดับทีม: <?php echo $arr_class_team[$row['class_team']]; ?><?php echo '</p>' ?>
								    <?php echo '</div>' ?>
									<?php echo '<div id="menu2" class="tab-pane fade">' ?>
									  <?php echo '<h5>' ?>สถานการณ์ประจำวัน<?php echo '</h5>' ?>
									  <?php echo '<p>' ?> การค้นหาผู้ป่วยเพิ่มเติม : <?php echo $arr_patient[$row['patient_search']]; ?>
									   <?php echo "<br>" ?>
									   การเก็บตัวอย่าง : <?php echo $arr_collecting1[$row['collecting1']]; ?>
									   <?php echo $arr_collecting2[$row['collecting2']]; ?>
									   <?php echo $arr_collecting3[$row['collecting3']]; ?>
									   <?php echo $arr_collecting4[$row['collecting4']]; ?>
									   <?php echo $arr_collecting5[$row['collecting5']]; ?>
									   <?php echo $row['disease_control']; ?>
									   <?php echo "<br>" ?>
									   สรุปสถานการณ์ : <?php echo $arr_summary1[$row['summary_situation1']]; ?>
									   <?php echo $arr_summary2[$row['summary_situation2']]; ?>
									   <?php echo $arr_summary3[$row['summary_situation3']]; ?>
									   <?php echo $arr_summary4[$row['summary_situation4']]; ?>
									   <?php echo $arr_summary5[$row['summary_situation5']]; ?>
									   <?php echo $row['other']; ?><?php echo '</p>' ?>
									<?php echo '</div>' ?>',
							   <?php echo $row['lat']; ?>, <?php echo $row['lng']; ?>,
							   '<?php echo $monthval[$row['month_travel']]; ?>',
							   '<?php echo $row['week_travel']; ?>',
						   		'<?php echo $row['week_return']; ?>',
							'<?php echo $row['class_team']; ?>',
							'<?php echo $row['date_entry']; ?>',
						],
						 <?php } ?>
						   ];
						   /**
							* Function to init map
							*/
						   function initialize() {
							   var center = new google.maps.LatLng(13.616413, 101.323434);
							   var mapOptions = {
								   zoom: 5,
								   center: center,
								   mapTypeId: google.maps.MapTypeId.ROADMAP

							   };
							   map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
							   for (i = 0; i < markers1.length; i++) {
								   addMarker(markers1[i]);
							   }

						   }
						   // Add a marker clusterer to manage the markers.
						   /**
							* Function to add marker to map
							*/
						   function addMarker(marker) {
							   var category = marker[4];
							   var category2 = marker[5];
							   var category3  = marker[6];
							   var category4  = marker[7];
							   var title = marker[0];
							   var pos = new google.maps.LatLng(marker[2], marker[3]);
							   var content = marker[1];
							   var icons = marker[0];

							   marker1 = new google.maps.Marker({
								   title: title,
								   position: pos,
								   category: category,
								   category2: category2,
								   category3: category3,
								   category4: category4,
								   map: map,
								   icon: title
							   });
							   gmarkers1.push(marker1);
							   // Marker click listener
							   google.maps.event.addListener(marker1, 'click', (function (marker1, content) {
					               return function () {
					                   console.log('Gmarker 1 gets pushed');
					                   infowindow.setContent(content);
					                   infowindow.open(map, marker1);
					                   map.panTo(this.getPosition());
					                   map.setZoom(15);
					               }
					           })(marker1, content));
						   }
						   /**
							* Function to filter markers by category
							*/
						   filterMarkers = function (category) {
							   for (i = 0; i < markers1.length; i++) {
								   marker = gmarkers1[i];
								   // If is same category or category not picked
								   if (marker.category == category || category.length === 0) {
									   marker.setVisible(true);
								   }
								   // Categories don't match
								   else {
									   marker.setVisible(false);
								   }
							   }
						   }
						   filterMarkers2 = function (category2) {
							   for (i = 0; i < markers1.length; i++) {
								   markerrr = gmarkers1[i];
								   // If is same category or category not picked
								   if (markerrr.category2 == category2 || category2.length === 0) {
									   markerrr.setVisible(true);
								   }
								   // Categories don't match
								   else {
									   markerrr.setVisible(false);
								   }
							   }
						   }
						   filterMarkers3 = function (category3) {
							   for (i = 0; i < markers1.length; i++) {
								   markerrrr = gmarkers1[i];
								   // If is same category or category not picked
								   if (markerrrr.category3 == category3 || category3.length === 0) {
									   markerrrr.setVisible(true);
								   }
								   // Categories don't match
								   else {
									   markerrrr.setVisible(false);
								   }
							   }
						   }
						   filterMarkers4 = function (category4) {
							   for (i = 0; i < markers1.length; i++) {
								   markerrrrr = gmarkers1[i];
								   // If is same category or category not picked
								   if (markerrrrr.category4 == category4 || category4.length === 0) {
									   markerrrrr.setVisible(true);
								   }
								   // Categories don't match
								   else {
									   markerrrrr.setVisible(false);
								   }
							   }
						   }
						   // Init map
						   initialize();
						 </script>
  </body>
  <script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js">
  </script>
  <!--   Core JS Files   -->
	<script src="Assets/js/jquery-1.10.2.js" type="text/javascript"></script>
	<script src="Assets/js/bootstrap.min.js" type="text/javascript"></script>
<!--  Checkbox, Radio & Switch Plugins -->
	<script src="Assets/js/bootstrap-checkbox-radio.js"></script>
<!--  Charts Plugin -->
	<script src="Assets/js/chartist.min.js"></script>
  <!--  Notifications Plugin    -->
	<script src="Assets/js/bootstrap-notify.js"></script>
  <!-- Paper Dashboard Core javascript and methods for Demo purpose -->
<!-- Paper Dashboard DEMO methods, don't include it in your project! -->
	<script src="Assets/js/demo.js"></script>
	<script src="Assets/js/jquery.sharrre.js"></script>
</html>
