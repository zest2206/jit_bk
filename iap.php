<?php
// session_start();
// require_once 'include/valUser.php';
$rand_idteam = time();
require_once 'include/vallocation.php';
require_once 'include/querys.php';
$register = new querys();


$regis = $register->getDataFromTable(
		'tbl_response', array(), array('WHERE Id_Team=?', 'ORDER BY Id_Team ASC'), array($_GET['Id_Team'])
		);

// $resive = $register->getDataFromTable(
// 	'tbl_person', array(), array('WHERE Id_Team=?', 'ORDER BY Id_Team ASC'), array($_GET['Id_Team'])
// 			 );
// $resivelocation = $register->getDataFromTable(
// 	'tbl_location', array(), array('WHERE Id_Team=?', 'ORDER BY Id_Team ASC'), array($_GET['Id_Team'])
// 			 );
//
// ?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<link rel="apple-touch-icon" sizes="76x76" href="Assets/img/apple-icon.png">
	<link rel="icon" type="image/png" sizes="96x96" href="Assets/img/favicon.png">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>Joint investigation team</title>
    <link href="Assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="Assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Paper Dashboard core CSS    -->
    <link href="Assets/css/paper-dashboard.css" rel="stylesheet"/>
    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <!-- <link href="Assets/css/demo.css" rel="stylesheet" /> -->
    <!--  Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>
    <link href="Assets/css/themify-icons.css" rel="stylesheet">
	<script src="Assets/jquery/jquery.min.js" type="text/javascript"></script>
				<!-- Paper Dashboard Core javascript and methods for Demo purpose -->
	<script src="Assets/js/paper-dashboard.js"></script>
			<!-- Paper Dashboard DEMO methods, don't include it in your project! -->
			<!-- <script src="Assets/js/demo.js"></script>
	<script src="Assets/js/jquery.sharrre.js"></script> -->
	<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
	<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<style>
@import url("https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css");


label{
	position: relative;
	cursor: pointer;
	color: #666;
	font-size: 30px;
}

input[type="checkbox"], input[type="radio"]{
	position: absolute;
	right: 9000px;
}

/*Check box*/
input[type="checkbox"] + .label-text:before{
	content: "\f096";
	font-family: "FontAwesome";
	speak: none;
	font-style: normal;
	font-weight: normal;
	font-variant: normal;
	text-transform: none;
	line-height: 1;
	-webkit-font-smoothing:antialiased;
	width: 1em;
	display: inline-block;
	margin-right: 5px;
}

input[type="checkbox"]:checked + .label-text:before{
	content: "\f14a";
	color: #2980b9;
	animation: effect 250ms ease-in;
}

input[type="checkbox"]:disabled + .label-text{
	color: #aaa;
}

input[type="checkbox"]:disabled + .label-text:before{
	content: "\f0c8";
	color: #ccc;
}

/*Radio box*/

input[type="radio"] + .label-text:before{
	content: "\f10c";
	font-family: "FontAwesome";
	speak: none;
	font-style: normal;
	font-weight: normal;
	font-variant: normal;
	text-transform: none;
	line-height: 1;
	-webkit-font-smoothing:antialiased;
	width: 1em;
	display: inline-block;
	margin-right: 5px;
}

input[type="radio"]:checked + .label-text:before{
	content: "\f192";
	color: #8e44ad;
	animation: effect 250ms ease-in;
}

input[type="radio"]:disabled + .label-text{
	color: #aaa;
}

input[type="radio"]:disabled + .label-text:before{
	content: "\f111";
	color: #ccc;
}

/*Radio Toggle*/

.toggle input[type="radio"] + .label-text:before{
	content: "\f204";
	font-family: "FontAwesome";
	speak: none;
	font-style: normal;
	font-weight: normal;
	font-variant: normal;
	text-transform: none;
	line-height: 1;
	-webkit-font-smoothing:antialiased;
	width: 1em;
	display: inline-block;
	margin-right: 10px;
}

.toggle input[type="radio"]:checked + .label-text:before{
	content: "\f205";
	color: #16a085;
	animation: effect 250ms ease-in;
}

.toggle input[type="radio"]:disabled + .label-text{
	color: #aaa;
}

.toggle input[type="radio"]:disabled + .label-text:before{
	content: "\f204";
	color: #ccc;
}


@keyframes effect{
	0%{transform: scale(0);}
	25%{transform: scale(1.3);}
	75%{transform: scale(1.4);}
	100%{transform: scale(1);}
}
</style>
</head>
<body>
<div class="wrapper">
	<div class="sidebar" data-background-color="white" data-active-color="danger">
    <!--
		Tip 1: you can change the color of the sidebar's background using: data-background-color="white | black"
		Tip 2: you can change the color of the active button using the data-active-color="primary | info | success | warning | danger"
	-->
    	<div class="sidebar-wrapper">
            <div class="logo">
                <a href="" class="simple-text">
                    JOINT INVESTIGATION TEAM
                </a>
            </div>

			<ul class="nav">
						 <li>
							 <a href="dashboard.php">
								 <i class="ti-panel"></i>
								 <p>Dashboard</p>
							 </a>
						 </li>
						 <li>
							 <a href="teamfrm.php">
								 <i class="ti-user"></i>
								 <p>กรอกข้อมูลทีมสอบสวนโรค</p>
							 </a>
						 </li>
				 <li class="active">
							 <a href="iaplst.php">
								 <i class="ti-marker"></i>
								 <p>รายงานสถานการณ์ประจำวัน</p>
							 </a>
						 </li>
						 <li>
							 <a href="http://www.boeeoc.moph.go.th/eventbase/calendar/zone99/">
								 <i class="ti-notepad"></i>
								 <p>ปฏิทินแจ้งข่าวการระบาด</p>
							 </a>
						 </li>
						 <li>
							 <a href="teamlst.php">
								 <i class="ti-view-list-alt"></i>
								 <p>ตารางทีมที่ออกสอบสวน</p>
							 </a>
						 </li>
						 <li>
							 <a href="http://203.157.15.110/boe/software/downloadtab.php#tab5">
								 <i class="ti-clipboard"></i>
								 <p>แบบฟอร์มสอบสวนโรค</p>
							 </a>
						 </li>
						 <li>
							 <a href="maps.php">
								 <i class="ti-map"></i>
								 <p>Maps</p>
							 </a>
						 </li>
				<li class="active-pro">
						 </li>
					 </ul>
    	</div>
    </div>
    <div class="main-panel">
		<nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar bar1"></span>
                        <span class="icon-bar bar2"></span>
                        <span class="icon-bar bar3"></span>
                    </button>
                    <a class="navbar-brand" href="#">รายงานสถานการณ์โรคประจำวัน</a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="ti-panel"></i>
								<p>Stats</p>
                            </a>
                        </li>
                        <li class="dropdown">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="ti-bell"></i>
                                    <p class="notification">5</p>
									<p>Notifications</p>
									<b class="caret"></b>
                              </a>
                              <ul class="dropdown-menu">
                                <li><a href="#">Notification 1</a></li>
                                <li><a href="#">Notification 2</a></li>
                                <li><a href="#">Notification 3</a></li>
                                <li><a href="#">Notification 4</a></li>
                                <li><a href="#">Another notification</a></li>
                              </ul>
                        </li>
						<li>
							<a href="logout.php">
									<i class="ti-power-off"></i>
								<p>logout</p>
                            </a>
                        </li>
                    </ul>

                </div>
            </div>
        </nav>
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="card">
                            <div class="header">
                            </div>
                            <div class="content">
                              <form name="register" role="form" action="iapPS.php" method="post" enctype="multipart/form-data" id="demoform">
                				<!--	<form name="register" role="form" action="addproductPS.php" method="post" enctype="multipart/form-data"> -->
								<fieldset>
									<div class="row">
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
											<div class="form-group required">
												<input type="hidden" name="Id_Team" id="Id_Team" class="form-control" placeholder="รหัสทีมสอบสวน" data-error="" value="<?php echo $regis[0]['Id_Team']; ?>" readonly>
												<h4><?php  $groupdisease=$regis[0]['groupdisease_name']; echo 'สถานการณ์ : '. $arr_groupdisease[$groupdisease]; ?><?php  $disease=$regis[0]['disease_name']; echo 'สถานการณ์ : '. $arr_disease[$disease]; ?></h4>
												<h4><?php  $province=$regis[0]['province_name']; echo 'จังหวัด : '. $arr_province[$province]; ?></h4>
												<h4><?php echo 'วันที่ : '.date("Y-m-d"); ?></h4>
												<div class="help-block with-errors"></div>
											</div>
										</div>
                				</div>
                				</fieldset>
                				<fieldset>
                					<legend class="navbar-brand">กิจกรรมที่ทำ</legend>
                					<div class="row">
											<!--Radio group-->
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
													<label for="title"  class="control-label"></label>
													<!--Radio group-->
												<div class="col-md-2">
													<div class="form-check">
														<label>
															<input type="checkbox" name="collecting1" value="1" checked> <span class="label-text">ค้นหาผู้ป่วย/ผู้สัมผัส</span>
														</label>
													</div>
												</div>
												<div class="col-md-2">
													<div class="form-check">
														<label>
															<input type="checkbox" name="collecting2" value="1"> <span class="label-text">คัดกรอง</span>
														</label>
													</div>
												</div>
												<div class="col-md-2">
													<div class="form-check">
														<label>
															<input type="checkbox" name="collecting3" value="1"> <span class="label-text">เก็บตัวอย่าง</span>
														</label>
													</div>
												</div>
												<div class="col-md-2">
													<div class="form-check">
														<label>
															<input type="checkbox" name="collecting4" value="1"> <span class="label-text">ทบทวนเวชระเบียน</span>
														</label>
													</div>
												</div>
												<div class="col-md-2">
													<div class="form-check">
														<label>
															<input type="checkbox" name="collecting5" value="1"> <span class="label-text">การควบคุมโรค</span>
														</label>
													</div>
												</div>
												<div class="col-md-6">
													<div class="form-group">
													<label>อื่นๆ</label>
														<textarea rows="5" class="form-control border-input" placeholder="กรุณากรอกรายละเอียดอื่นๆ"  name="disease_control" id="disease_control"></textarea>
													</div>
												</div>
													<!--Radio group-->
					</div>
						</div>
                				</fieldset>
								<fieldset>
									<legend class="navbar-brand">ผลการสอบสวน</legend>
									<div class="row">
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
													<div class="col-md-12">
															<label for="title"  class="control-label">ยืนยันการวินิจฉัย</label>
															<!--Radio group-->
														<div class="col-md-12">
															<div class="form-check">
										   					<div class="col-md-2">
																<label>
																	<input type="checkbox" name="summary_situation1" value="1" checked> <span class="label-text">มี</span>
																</label>
															</div>
														</div>
															<div class="form-check">
															<div class="col-md-2">
																<label>
																	<input type="checkbox" name="summary_situation2" value="1"> <span class="label-text">ไม่มี</span>
																</label>
															</div>
														   </div>
													   </div>
														   <div class="col-md-12">
																   <label for="title"  class="control-label">ขนาดปัญหา</label>
																   <!--Radio group-->
																   <div class="form-check">
																	   <label>
																		   <input type="text"  class="form-control border-input" name="summary_situation1" placeholder="จำนวนผู้ป่วยสะสม" checked>
																	   </label>
																   </div>
																   <div class="form-check">
																	   <label>
																		   <input type="text"  class="form-control border-input" name="summary_situation1" placeholder="จำนวนผู้ป่วยรายใหม่" checked>
																	   </label>
																   </div>
																   <div class="form-check">
																	   <label>
																		   <input type="text"  class="form-control border-input" name="summary_situation1" placeholder="อัตราป่วย" checked>
																	   </label>
																   </div>
																  </div>
																  <div class="col-md-12">
																		  <label for="title"  class="control-label">ความรุนแรง</label>
																		  <!--Radio group-->
																		  <div class="form-check">
																			   <div class="col-md-2">
																			  <label>
																				  <input type="checkbox" name="summary_situation1" value="1" checked> <span class="label-text">ตาย</span>
																			  </label>
																		  </div>
																		 		  </div>
																				  <div class="form-check">
																					   <div class="col-md-2">
																					  <label>
																						  <input type="checkbox" name="summary_situation1" value="1" checked> <span class="label-text">อื่นๆ</span>
																					  </label>
																				  </div>
																				 		  </div>
																		 </div>
																	<div class="col-md-4">
																		<div class="form-group">
																		<label>แนวโน้ม</label>
																			<textarea rows="5" class="form-control border-input" placeholder="กรุณากรอกรายละเอียดอื่นๆ"  name="other" id="other"></textarea>
																		</div>
																	</div>
															<!--Radio group-->
														</div>
								</div>
								</fieldset>
								<fieldset>
									<legend class="navbar-brand">ปัญหาที่พบ</legend>
									<div class="row">
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
													<div class="col-md-12">
															<label for="title"  class="control-label"></label>
															<!--Radio group-->
															<div class="form-check">
																<div class="col-md-2">
																<label>
																	<input type="checkbox" name="summary_situation1" value="1" checked> <span class="label-text">คนไม่พอ</span>
																</label>
															</div>
															</div>
															<div class="form-check">
																<div class="col-md-2">
																<label>
																	<input type="checkbox" name="summary_situation2" value="1"> <span class="label-text">อุปกรณ์ไม่พอ</span>
																</label>
																</div>
															</div>
															<div class="form-check">
																<div class="col-md-2">
																<label>
																	<input type="checkbox" name="summary_situation3" value="1"> <span class="label-text">ข้อกฏหมาย</span>
																</label>
															</div>
															</div>
															<div class="form-check">
																<div class="col-md-2">
																<label>
																	<input type="checkbox" name="summary_situation4" value="1"> <span class="label-text">ความร่วมมือ</span>
																</label>
															</div>
															</div>
															<div class="form-check">
																<div class="col-md-2">
																<label>
																	<input type="checkbox" name="summary_situation5" value="1"> <span class="label-text">อื่นๆ</span>
																</label>
															</div>
															</div>
															<div class="col-md-4">
																<div class="form-group">
																<label>การสนันสนุนที่ต้องการ</label>
																	<textarea rows="5" class="form-control border-input" placeholder="การสนันสนุนที่ต้องการ"  name="other" id="other"></textarea>
																</div>
																<div class="form-group">
																<label>อื่นๆ</label>
																	<textarea rows="5" class="form-control border-input" placeholder="กรุณากรอกรายละเอียดอื่นๆ"  name="other" id="other"></textarea>
																</div>
															</div>
														   </div>
															<!--Radio group-->
							</div>
								</div>
								</fieldset>
								<fieldset>
									<legend class="navbar-brand">แผนการทำงาน</legend>
									<div class="row">
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
													<div class="col-md-12">
															<label for="title"  class="control-label"></label>
															<!--Radio group-->
															<div class="form-check">
																<div class="col-md-2">
																<label>
																	<input type="checkbox" name="summary_situation1" value="1" checked> <span class="label-text">มีแนวโน้มผู้ป่วยเพิ่มขึ้น</span>
																</label>
																</div>
															</div>
															<div class="form-check">
																<div class="col-md-2">
																<label>
																	<input type="checkbox" name="summary_situation2" value="1"> <span class="label-text">มีแนวโน้มระบาดในวงกว้าง (มากกว่า 2 พื้นที่)</span>
																</label>
															</div>
															</div>
															<div class="form-check">
																<div class="col-md-2">
																<label>
																	<input type="checkbox" name="summary_situation3" value="1"> <span class="label-text">สามารถควบคุโรคได้</span>
																</label>
																</div>
															</div>
															<div class="form-check">
																<div class="col-md-2">
																<label>
																	<input type="checkbox" name="summary_situation4" value="1"> <span class="label-text">ยังไม่สามารถประเมินสถานการณ์ได้</span>
																</label>
																</div>
															</div>
															<div class="form-check">
																<div class="col-md-2">
																<label>
																	<input type="checkbox" name="summary_situation5" value="1"> <span class="label-text">อื่นๆ</span>
																</label>
																</div>
															</div>
														   </div>
																<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
																	<div class="col-md-6">
																		<div class="form-group">
																		<label>ชื่อผู้บันทึกข้อมูลประจำวัน</label>
																				<input type="text" name="Id_Team" id="Id_Team"  class="form-control" placeholder="รหัสทีมสอบสวน" data-error="" required>
																		</div>
																	</div>
																</div>
															<!--Radio group-->
							</div>
								</div>
								</fieldset>
                				<fieldset>
								<legend class="navbar-brand"></legend>
								<div class="col-md-3">
								</div>
								<div class="col-md-3">
										<a href="iapreport.php"  class="btn btn-danger btn-block btn-fill">ยกเลิก</a>
								</div>
								<div class="col-md-3">
											<button type="submit"  name="buttons" class="btn btn-success btn-block btn-fill">บันทึกข้อมูล</button>
                							<span id="xhr-progress" style="display: none;"><img src="Assets/icons/ajax-loader.gif"> กำลังบันทึกข้อมูล โปรดรอ...</span>
								</div>
								<div class="col-md-3">
								</div>
										</fieldset>
								</div>
                					</form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <footer class="footer">
            <div class="container-fluid">
				<div class="copyright pull-right">
                    &copy; <script>document.write(new Date().getFullYear())</script></i> by <a href="http://203.157.15.110/boe/">CEI</a>
                </div>
            </div>
        </footer>
		        </div>
	    <!-- นำเข้า Javascript jQuery -->
	    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	    <!-- Include all compiled plugins (below), or include individual files as needed -->
	    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/js/select2.min.js"></script>
			<!-- hidden textbox -->
					<script type="text/javascript">
							$(function () {
									$("#ddlPassport").change(function () {
											if ($(this).val() == "6") {
													$("#dvPassport").show();
											} else {
													$("#dvPassport").hide();
											}
									});
							});
					</script>
					<!-- end hidden textbox -->

</body>
</html>
