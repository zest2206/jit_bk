<?php
require_once 'include/querys.php';
$register = new querys();


$regis = $register->getDataFromTable(
		'tbl_response', array(), array('WHERE Id_Team=?', 'ORDER BY Id_Team ASC'), array($_GET['Id_Team'])
		);
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<link rel="apple-touch-icon" sizes="76x76" href="Assets/img/apple-icon.png">
	<link rel="icon" type="image/png" sizes="96x96" href="Assets/img/favicon.png">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>Joint investigation team</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />

    <!--  Social tags      -->
    <meta name="keywords" content="bootstrap dashboard, creative tim, html dashboard, html css dashboard, web dashboard, paper design, bootstrap dashboard, bootstrap, css3 dashboard, bootstrap admin, paper bootstrap dashboard, frontend, responsive bootstrap dashboard">

    <meta name="description" content="Paper Dashboard is a beautiful Bootstrap Admin Panel for your next project.">

    <!-- Schema.org markup for Google+ -->
    <meta itemprop="name" content="Paper Dashboard by Creative Tim">
    <meta itemprop="description" content="Paper Dashboard is a beautiful Bootstrap Admin Panel for your next project.">
    <meta itemprop="image" content="http://s3.amazonaws.com/creativetim_bucket/products/43/original/opt_pd_thumbnail.jpg">

    <!-- Twitter Card data -->
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="@creativetim">
    <meta name="twitter:title" content="Paper Dashboard PRO by Creative Tim">
    <meta name="twitter:description" content="Paper Dashboard is a beautiful Bootstrap Admin Panel for your next project.">
    <meta name="twitter:creator" content="@creativetim">
    <meta name="twitter:image" content="http://s3.amazonaws.com/creativetim_bucket/products/43/original/opt_pd_thumbnail.jpg">

    <!-- Open Graph data -->
    <meta property="og:title" content="Paper Dashboard by Creative Tim" />
    <meta property="og:type" content="article" />
    <meta property="og:url" content="http://demos.creative-tim.com/paper-dashboard/dashboard.html" />
    <meta property="og:image" content="http://s3.amazonaws.com/creativetim_bucket/products/43/original/opt_pd_thumbnail.jpg"/>
    <meta property="og:description" content="Paper Dashboard is a beautiful Bootstrap Admin Panel for your next project." />
    <meta property="og:site_name" content="Creative Tim" />


    <!-- Bootstrap core CSS     -->
    <link href="Assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="Assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Paper Dashboard core CSS    -->
    <link href="Assets/css/paper-dashboard.css" rel="stylesheet"/>


    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <!-- <link href="Assets/css/demo.css" rel="stylesheet" /> -->


    <!--  Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>
    <link href="Assets/css/themify-icons.css" rel="stylesheet">

		<script src="Assets/jquery/jquery.min.js" type="text/javascript"></script>




				<!-- Paper Dashboard Core javascript and methods for Demo purpose -->
			<script src="Assets/js/paper-dashboard.js"></script>
			<!-- Paper Dashboard DEMO methods, don't include it in your project! -->
			<!-- <script src="Assets/js/demo.js"></script>
			<script src="Assets/js/jquery.sharrre.js"></script> -->
</head>
<body>

<div class="wrapper">
	<div class="sidebar" data-background-color="white" data-active-color="danger">

    <!--
		Tip 1: you can change the color of the sidebar's background using: data-background-color="white | black"
		Tip 2: you can change the color of the active button using the data-active-color="primary | info | success | warning | danger"
	-->

    	<div class="sidebar-wrapper">
            <div class="logo">
                <a href="" class="simple-text">
                    JOINT INVESTIGATION TEAM
                </a>
            </div>

			<ul class="nav">
						 <li>
							 <a href="dashboard.php">
								 <i class="ti-panel"></i>
								 <p>Dashboard</p>
							 </a>
						 </li>
						 <li class="active">
							 <a href="teamfrm.php">
								 <i class="ti-user"></i>
								 <p>กรอกข้อมูลทีมสอบสวนโรค</p>
							 </a>
						 </li>
				 <li>
							 <a href="iaplst.php">
								 <i class="ti-marker"></i>
								 <p>รายงานสถานการณ์ประจำวัน</p>
							 </a>
						 </li>
						 <li>
							 <a href="http://www.boeeoc.moph.go.th/eventbase/calendar/zone99/">
								 <i class="ti-notepad"></i>
								 <p>ปฏิทินแจ้งข่าวการระบาด</p>
							 </a>
						 </li>
						 <li>
							 <a href="teamlst.php">
								 <i class="ti-view-list-alt"></i>
								 <p>ตารางทีมที่ออกสอบสวน</p>
							 </a>
						 </li>
						 <li>
							 <a href="http://203.157.15.110/boe/software/downloadtab.php#tab5">
								 <i class="ti-clipboard"></i>
								 <p>แบบฟอร์มสอบสวนโรค</p>
							 </a>
						 </li>
						 <li>
							 <a href="maps.php">
								 <i class="ti-map"></i>
								 <p>Maps</p>
							 </a>
						 </li>
				<li class="active-pro">
						 </li>
					 </ul>
    	</div>
    </div>

    <div class="main-panel">
		<nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar bar1"></span>
                        <span class="icon-bar bar2"></span>
                        <span class="icon-bar bar3"></span>
                    </button>
                    <a class="navbar-brand" href="#">ข้อมูลโรคและสถานที่ออกสอบสวนโรค</a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="ti-panel"></i>
								<p>Stats</p>
                            </a>
                        </li>
                        <li class="dropdown">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="ti-bell"></i>
                                    <p class="notification">5</p>
									<p>Notifications</p>
									<b class="caret"></b>
                              </a>
                              <ul class="dropdown-menu">
                                <li><a href="#">Notification 1</a></li>
                                <li><a href="#">Notification 2</a></li>
                                <li><a href="#">Notification 3</a></li>
                                <li><a href="#">Notification 4</a></li>
                                <li><a href="#">Another notification</a></li>
                              </ul>
                        </li>
												<li>
													<a href="logout.php">
															<i class="ti-power-off"></i>
														<p>logout</p>
						                            </a>
						                        </li>
                    </ul>

                </div>
            </div>
        </nav>


        <div class="content">
            <div class="container-fluid">
                <div class="row">

                    <div class="col-lg-12 col-md-12">
                        <div class="card">
                            <div class="header">
                            </div>
                            <div class="content">
															<form id="BZ1127">
															<label for="hZ2927">Address</label>
															<div class="col-3 m3">
															<input id="hZ2927" type="text" class="width70" placeholder="Type address here to get lat long" required />
															<button title="Find lat long coordinates" class="button">Find</button><br/>
															  </div>
															</form>
															<form name="register" role="form" action="locationPS.php" method="post" enctype="multipart/form-data" id="demoform">
																<fieldset>
																	<legend class="navbar-brand">รหัสทีมสอบสวน</legend>
																	<div class="row">
																		<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
																			<div class="form-group required">
																				<label for="Id_Team">รหัสทีมสอบสวนโรค</label>
																				<input type="text" name="Id_Team"  value="<?php echo $regis[0]['Id_Team']; ?>" class="form-control" required readonly/></td>
																			</div>
																		</div>
																		<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
																			<div class="form-group required">
																				<label for="lat">Latitude</label>
																				<input type="text" name="lat" id="lat" placeholder="lat coordinate" /></div>
																		</div>
																		<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
																			<div class="form-group required">
																				<label for="lng">Longitude</label>
																				<input type="text" name="lng" id="lng" placeholder="long coordinate" />
																		</div>
																</div>
																</fieldset>
																<fieldset>
																  <button type="submit"  name="buttons" class="btn btn-danger">บันทึกข้อมูล</button>
																  <span id="xhr-progress" style="display: none;"><img src="Assets/icons/ajax-loader.gif"> กำลังบันทึกข้อมูล โปรดรอ...</span>
																</fieldset>
                				<!--	<form name="register" role="form" action="addproductPS.php" method="post" enctype="multipart/form-data"> -->
                					</form>
													<fieldset>
														<div id="latlongmap" style="height:450px;"></div>
													</fieldset>
													<fieldset>
														<h3 class="titleh3">Lat Long</h3>
														<span id="latlngspan" class="coordinatetxt">0,0</span>
													</fieldset>
													<fieldset>
														<h3 class="titleh3">Map Mouse Over Location</h3>
														<span id="mlat" class="coordinatetxt">0,0</span>
													</fieldset>
													<fieldset>
													<div class="margin20">
													<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
													<!-- LatLongMiddle -->
													<ins class="adsbygoogle"
													     style="display:block"
													     data-ad-client="ca-pub-4342808821632378"
													     data-ad-slot="1760724841"
													     data-ad-format="auto"></ins>
													<script>
													(adsbygoogle = window.adsbygoogle || []).push({});
													</script>
													</div>
												</fieldset>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>


        <footer class="footer">
            <div class="container-fluid">
				<div class="copyright pull-right">
                    &copy; <script>document.write(new Date().getFullYear())</script></i> by <a href="http://203.157.15.110/boe/">CEI</a>
                </div>
            </div>
        </footer>

    </div>
</div>
<script type="application/ld+json">{"@context":"http://schema.org","@type":"WebSite","name":"Lat Long","alternateName":"Latitude and Longitude","url":"https://www.latlong.net/","sameAs":["https://plus.google.com/+LatlongNet","https://twitter.com/latlong_net"]}</script>
	<script>var head=document.getElementsByTagName("head")[0],insertBefore=head.insertBefore;head.insertBefore=function(e,o){e.href&&0===e.href.indexOf("https://fonts.googleapis.com/css?family=Roboto")||insertBefore.call(head,e,o)};
	var yid ='address-lat-long';
	function gotop(){return document.body.scrollTop=document.documentElement.scrollTop=0,!1}window.onscroll=function(){var o=document.getElementsByTagName("body")[0].scrollTop;o>=100?document.getElementById("cd-top").className="cd-top cd-is-visible":document.getElementById("cd-top").className="cd-top"};
	function pscm(e){e.preventDefault&&e.preventDefault();var t=document.getElementById("commentname").value,n=document.getElementById("commenttext").value,m=new XMLHttpRequest,a="commentname="+encodeURIComponent(t)+"&yid="+encodeURIComponent(yid)+"&commenttext="+encodeURIComponent(n);return m.open("POST","/_addcomment.php",!0),m.setRequestHeader("Content-type","application/x-www-form-urlencoded"),m.onreadystatechange=function(){4===m.readyState&&200===m.status?(document.getElementById("tagmessage").innerHTML="Your comment saved successfully and will publish after approval.",document.getElementById("commentname").value="",document.getElementById("commenttext").value=""):document.getElementById("tagmessage").innerHTML="There was an error, please try again later.",document.getElementById("tagmessage").style.visibility="visible"},m.send(a),!1}var frmcomment=document.getElementById("frmcomment");frmcomment.attachEvent?frmcomment.attachEvent("submit",pscm):frmcomment.addEventListener("submit",pscm);
	</script>
<script>
!function(e,t)
{function n(){t.getElementById("navmenu").classList.toggle("list-vertical"),t.getElementsByTagName("header")[0].classList.toggle("open"),t.getElementsByTagName("body")[0].classList.toggle("open")}function s(){o.classList.contains("open")?setTimeout(n,500):n(),o.classList.toggle("open"),t.getElementsByTagName("main")[0].classList.toggle("open")}function i(){o.classList.contains("open")&&s()}var o=t.getElementById("menu"),a="onorientationchange"in e?"orientationchange":"resize";t.getElementById("toggle").addEventListener("click",function(){s()}),e.addEventListener(a,i)}(this,this.document);</script><script type="text/javascript">
function initialize() {
var e = new google.maps.LatLng(13.8503967, 100.5283214), t = {zoom: 7, center: e, panControl: !0, scrollwheel: !1, scaleControl: !0, overviewMapControl: !0, overviewMapControlOptions: {opened: !0}, mapTypeId: google.maps.MapTypeId.HYBRID};
map = new google.maps.Map(document.getElementById("latlongmap"), t), geocoder = new google.maps.Geocoder, marker = new google.maps.Marker({position: e, map: map}), map.streetViewControl = !1, infowindow = new google.maps.InfoWindow({content: "(1.10, 1.10)"}), google.maps.event.addListener(map, "click", function (e) {
marker.setPosition(e.latLng);
var t = e.latLng, o = "(" + t.lat().toFixed(6) + ", " + t.lng().toFixed(6) + ")";
infowindow.setContent(o), document.getElementById("lat").value = t.lat().toFixed(6), document.getElementById("lng").value = t.lng().toFixed(6), document.getElementById("latlngspan").innerHTML = o, document.getElementById("coordinatesurl").value = "https://www.latlong.net/c/?lat=" + t.lat().toFixed(6) + "&long=" + t.lng().toFixed(6), document.getElementById("coordinateslink").innerHTML = '&lt;a href="https://www.latlong.net/c/?lat=' + t.lat().toFixed(6) + "&amp;long=" + t.lng().toFixed(6) + '" target="_blank"&gt;(' + t.lat().toFixed(6) + ", " + t.lng().toFixed(6) + ")&lt;/a&gt;", dec2dms()
}), google.maps.event.addListener(map, "mousemove", function (e) {
var t = e.latLng;
document.getElementById("mlat").innerHTML = "(" + t.lat().toFixed(6) + ", " + t.lng().toFixed(6) + ")"
})
}
function codeAddress(e) {
e.preventDefault && e.preventDefault();
var t = document.getElementById("hZ2927").value;
return"" === t ? void alert("Address can not be empty!") : void geocoder.geocode({address: t}, function (e, t) {
if (t === google.maps.GeocoderStatus.OK) {
map.setCenter(e[0].geometry.location), document.getElementById("lat").value = e[0].geometry.location.lat().toFixed(6), document.getElementById("lng").value = e[0].geometry.location.lng().toFixed(6);
var o = "(" + e[0].geometry.location.lat().toFixed(6) + ", " + +e[0].geometry.location.lng().toFixed(6) + ")";
document.getElementById("latlngspan").innerHTML = o, document.getElementById("coordinatesurl").value = "https://www.latlong.net/c/?lat=" + e[0].geometry.location.lat().toFixed(6) + "&long=" + e[0].geometry.location.lng().toFixed(6), document.getElementById("coordinateslink").innerHTML = '&lt;a href="https://www.latlong.net/c/?lat=' + e[0].geometry.location.lat().toFixed(6) + "&amp;long=" + e[0].geometry.location.lng().toFixed(6) + '" target="_blank"&gt;(' + e[0].geometry.location.lat().toFixed(6) + ", " + e[0].geometry.location.lng().toFixed(6) + ")&lt;/a&gt;", marker.setPosition(e[0].geometry.location), map.setZoom(16), infowindow.setContent(o), infowindow && infowindow.close(), google.maps.event.addListener(marker, "click", function () {
infowindow.open(map, marker)
}), infowindow.open(map, marker), dec2dms()
} else
alert("Lat and long cannot be found.")
})
}
var latlongform = document.getElementById("BZ1127");
latlongform.attachEvent ? latlongform.attachEvent("submit", codeAddress) : latlongform.addEventListener("submit", codeAddress);
var map, geocoder, marker, infowindow;
function dec2dms() {
var e = document.getElementById("lat").value, t = document.getElementById("lng").value;
document.getElementById("dms-lat").innerHTML = getdms(e, !0), document.getElementById("dms-lng").innerHTML = getdms(t, !1)
}
function getdms(e, t) {
var n = 0, m = 0, l = 0, a = "X";
return a = t && 0 > e ? "S" : !t && 0 > e ? "W" : t ? "N" : "E", d = Math.abs(e), n = Math.floor(d), l = 3600 * (d - n), m = Math.floor(l / 60), l = Math.round(1e4 * (l - 60 * m)) / 1e4, n + "&deg; " + m + "' " + l + "'' " + a
}
</script>
<script src="https://maps.googleapis.com/maps/api/js?callback=initialize&amp;key=AIzaSyCAP4jkz9nPhjRqMskknIor2prtqs8dcK4" async defer></script>

</body>
</html>
