<?php
 require_once 'include/querys.php';
 require_once 'include/valdisease.php';
 require_once 'include/valprovince.php';
 require_once 'include/valdistrict.php';
 require_once 'include/valamphures.php';
 // require_once 'include/conf.php';
 $query = new querys();
 $regis = $query->getDataFromTable(
 		'tbl_person', array(), array('WHERE Id_Team=?', 'ORDER BY Id_Team ASC'), array($_GET['Id_Team'])
 		);


// var_dump ($regis);


 ?>
 <!doctype html>
 <html lang="en">
 <head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="Assets/img/apple-icon.png">
  <link rel="icon" type="image/png" sizes="96x96" href="Assets/img/favicon.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

  <title>Joint investigation team</title>

  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
     <meta name="viewport" content="width=device-width" />

     <!--  Social tags      -->
     <meta name="keywords" content="bootstrap dashboard, creative tim, html dashboard, html css dashboard, web dashboard, paper design, bootstrap dashboard, bootstrap, css3 dashboard, bootstrap admin, paper bootstrap dashboard, frontend, responsive bootstrap dashboard">

     <meta name="description" content="Paper Dashboard is a beautiful Bootstrap Admin Panel for your next project.">

     <!-- Schema.org markup for Google+ -->
     <meta itemprop="name" content="Paper Dashboard by Creative Tim">
     <meta itemprop="description" content="Paper Dashboard is a beautiful Bootstrap Admin Panel for your next project.">
     <meta itemprop="image" content="http://s3.amazonaws.com/creativetim_bucket/products/43/original/opt_pd_thumbnail.jpg">

     <!-- Twitter Card data -->
     <meta name="twitter:card" content="summary_large_image">
     <meta name="twitter:site" content="@creativetim">
     <meta name="twitter:title" content="Paper Dashboard PRO by Creative Tim">
     <meta name="twitter:description" content="Paper Dashboard is a beautiful Bootstrap Admin Panel for your next project.">
     <meta name="twitter:creator" content="@creativetim">
     <meta name="twitter:image" content="http://s3.amazonaws.com/creativetim_bucket/products/43/original/opt_pd_thumbnail.jpg">

     <!-- Open Graph data -->
     <meta property="og:title" content="Paper Dashboard by Creative Tim" />
     <meta property="og:type" content="article" />
     <meta property="og:url" content="http://demos.creative-tim.com/paper-dashboard/dashboard.html" />
     <meta property="og:image" content="http://s3.amazonaws.com/creativetim_bucket/products/43/original/opt_pd_thumbnail.jpg"/>
     <meta property="og:description" content="Paper Dashboard is a beautiful Bootstrap Admin Panel for your next project." />
     <meta property="og:site_name" content="Creative Tim" />


     <!-- Bootstrap core CSS     -->
     <link href="Assets/css/bootstrap.min.css" rel="stylesheet" />

     <!-- Animation library for notifications   -->
     <link href="Assets/css/animate.min.css" rel="stylesheet"/>

     <!--  Paper Dashboard core CSS    -->
     <link href="Assets/css/paper-dashboard.css" rel="stylesheet"/>


     <!--  CSS for Demo Purpose, don't include it in your project     -->
     <!-- <link href="Assets/css/demo.css" rel="stylesheet" /> -->


     <!--  Fonts and icons     -->
     <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
     <link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>
     <link href="Assets/css/themify-icons.css" rel="stylesheet">
    <link rel="stylesheet"href="https://google-developers.appspot.com/_static/0d76052693/css/devsite-cyan.css">
    <script src="https://google-developers.appspot.com/_static/0d76052693/js/prettify-bundle.js"></script>
    <script src="https://google-developers.appspot.com/_static/0d76052693/js/jquery-bundle.js"></script>
    <script src="//www.google.com/jsapi?key=AIzaSyCZfHRnq7tigC-COeQRmoa9Cxr0vbrK6xw"></script>
    <script src="https://google-developers.appspot.com/_static/0d76052693/js/framebox.js"></script>

    </head>
    <body>

    <div class="wrapper">
        <div class="sidebar" data-background-color="white" data-active-color="danger">

        <!--
        Tip 1: you can change the color of the sidebar's background using: data-background-color="white | black"
        Tip 2: you can change the color of the active button using the data-active-color="primary | info | success | warning | danger"
      -->

          <div class="sidebar-wrapper">
                <div class="logo">
                    <a href="" class="simple-text">
    Joint investigation team
                    </a>
                </div>

				<ul class="nav">
							 <li>
								 <a href="dashboard.php">
									 <i class="ti-panel"></i>
									 <p>Dashboard</p>
								 </a>
							 </li>
							 <li class="active">
								 <a href="teamfrm.php">
									 <i class="ti-user"></i>
									 <p>กรอกข้อมูลทีมสอบสวนโรค</p>
								 </a>
							 </li>
					 <li>
								 <a href="iaplst.php">
									 <i class="ti-marker"></i>
									 <p>รายงานสถานการณ์ประจำวัน</p>
								 </a>
							 </li>
							 <li>
								 <a href="http://www.boeeoc.moph.go.th/eventbase/calendar/zone99/">
									 <i class="ti-notepad"></i>
									 <p>ปฏิทินแจ้งข่าวการระบาด</p>
								 </a>
							 </li>
							 <li>
								 <a href="teamlst.php">
									 <i class="ti-view-list-alt"></i>
									 <p>ตารางทีมที่ออกสอบสวน</p>
								 </a>
							 </li>
							 <li>
								 <a href="http://203.157.15.110/boe/software/downloadtab.php#tab5">
									 <i class="ti-clipboard"></i>
									 <p>แบบฟอร์มสอบสวนโรค</p>
								 </a>
							 </li>
							 <li>
								 <a href="maps.php">
									 <i class="ti-map"></i>
									 <p>Maps</p>
								 </a>
							 </li>
					<li class="active-pro">
							 </li>
						 </ul>
          </div>
        </div>

        <div class="main-panel">
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar bar1"></span>
                            <span class="icon-bar bar2"></span>
                            <span class="icon-bar bar3"></span>
                        </button>
                        <a class="navbar-brand" href="#">Maps</a>
                    </div>
                    <div class="collapse navbar-collapse">
                        <ul class="nav navbar-nav navbar-right">
                            <li>
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="ti-panel"></i>
                    <p>Stats</p>
                                </a>
                            </li>
                            <li class="dropdown">
                                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                        <i class="ti-bell"></i>
                                        <p class="notification">5</p>
                      <p>Notifications</p>
                      <b class="caret"></b>
                                  </a>
                                  <ul class="dropdown-menu">
                                    <li><a href="#">Notification 1</a></li>
                                    <li><a href="#">Notification 2</a></li>
                                    <li><a href="#">Notification 3</a></li>
                                    <li><a href="#">Notification 4</a></li>
                                    <li><a href="#">Another notification</a></li>
                                  </ul>
                            </li>
                <li>
                                <a href="#">
                    <i class="ti-settings"></i>
                    <p>Settings</p>
                                </a>
                            </li>
                        </ul>

                    </div>
                </div>
            </nav>

            <div class="content">
                   <div class="container-fluid">
                       <div class="row">
                           <div class="col-md-12">
                               <div class="card">
                                   <div class="content">
      <table id="example" class="hover" cellspacing="0" width="100%">
        <thead>
          <tr>
            <th>Id_Team</th>
            <th>ชื่อ-นามสกุล</th>
            <th>หน่วยงาน</th>
            <th>ตำแหน่ง</th>
          </tr>
        </thead>
        <tfoot></tfoot>
        <tbody>
        <?php
foreach ($regis as $k => $v) {

          echo "<tr>\n";
            echo "<td>".$v['Id_Team']."</td>\n";
            echo "<td>".$v['name_surname']."</td>\n";
            echo "<td>".$v['division']."</td>\n";
            echo "<td>".$v['position']."</td>\n";
          echo "</tr>\n";
}
        ?>
        </tbody>
      </table>
    </div>

<script>
$(document).ready(function() {
  $(document).ready(function() {
      $('#example').DataTable();
  } );
});
</script>
</body>
</html>
