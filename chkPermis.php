<?php
session_start();
if ($_SESSION['chkSessId'] != session_id()) {
	session_destroy();
	header('Location: http://www.boeeoc.moph.go.th');
	exit;
}
require_once 'include/conf.php';
if (isset($_GET['uid']) && isset($_GET['cid'])) {
	$_SESSION['uid'] = $_GET['uid'];
	$_SESSION['cid'] = $_GET['cid'];
	$_SESSION['cuid'] = $_GET['uid'].'xit0ydiN';

	$dbh_user = new PDO($dsn_user, $username, $password, $options);
	$dbh_user->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

	$sth = $dbh_user->prepare("SELECT * FROM admin WHERE ID=? LIMIT 1");
	$sth->execute(array($_GET['uid']));
	$result = $sth->fetchAll();
	if (sizeof($result) > 0) {
		header('Location: teamfrm.php');
		exit;
	}
} else {
	session_destroy();
	header('Location: http://www.boeeoc.moph.go.th');
	exit;
}
?>
