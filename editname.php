<?php
require 'include/db.php';
require_once 'include/valposition.php';
require_once 'include/querys.php';
$register = new querys();
$person = $register->getDataFromTable(
		'tbl_person', array(), array('WHERE id=?', 'ORDER BY id ASC'), array($_GET['id'])
		);
?>
<!doctype html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<link rel="apple-touch-icon" sizes="76x76" href="Assets/img/apple-icon.png">
		<link rel="icon" type="image/png" sizes="96x96" href="Assets/img/favicon.png">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<title>Joint investigation team</title>
		<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
		<meta name="viewport" content="width=device-width" />
<!--  Social tags      -->
		<meta name="keywords" content="bootstrap dashboard, creative tim, html dashboard, html css dashboard, web dashboard, paper design, bootstrap dashboard, bootstrap, css3 dashboard, bootstrap admin, paper bootstrap dashboard, frontend, responsive bootstrap dashboard">
		<meta name="description" content="Paper Dashboard is a beautiful Bootstrap Admin Panel for your next project.">
<!-- Schema.org markup for Google+ -->
		<meta itemprop="name" content="Paper Dashboard by Creative Tim">
		<meta itemprop="description" content="Paper Dashboard is a beautiful Bootstrap Admin Panel for your next project.">
		<meta itemprop="image" content="http://s3.amazonaws.com/creativetim_bucket/products/43/original/opt_pd_thumbnail.jpg">
<!-- Twitter Card data -->
		<meta name="twitter:card" content="summary_large_image">
		<meta name="twitter:site" content="@creativetim">
		<meta name="twitter:title" content="Paper Dashboard PRO by Creative Tim">
		<meta name="twitter:description" content="Paper Dashboard is a beautiful Bootstrap Admin Panel for your next project.">
		<meta name="twitter:creator" content="@creativetim">
		<meta name="twitter:image" content="http://s3.amazonaws.com/creativetim_bucket/products/43/original/opt_pd_thumbnail.jpg">
<!-- Open Graph data -->
		<meta property="og:title" content="Paper Dashboard by Creative Tim" />
		<meta property="og:type" content="article" />
		<meta property="og:url" content="http://demos.creative-tim.com/paper-dashboard/dashboard.html" />
		<meta property="og:image" content="http://s3.amazonaws.com/creativetim_bucket/products/43/original/opt_pd_thumbnail.jpg"/>
		<meta property="og:description" content="Paper Dashboard is a beautiful Bootstrap Admin Panel for your next project." />
		<meta property="og:site_name" content="Creative Tim" />
<!-- Bootstrap core CSS     -->
		<link href="Assets/css/bootstrap.min.css" rel="stylesheet" />
<!-- Animation library for notifications   -->
		<link href="Assets/css/animate.min.css" rel="stylesheet"/>
<!--  Paper Dashboard core CSS    -->
		<link href="Assets/css/paper-dashboard.css" rel="stylesheet"/>ฃ
<!--  CSS for Demo Purpose, don't include it in your project     -->
<!-- <link href="Assets/css/demo.css" rel="stylesheet" /> -->
<!--  Fonts and icons     -->
		<link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
		<link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>
		<link href="Assets/css/themify-icons.css" rel="stylesheet">
		<script src="Assets/jquery/jquery.min.js" type="text/javascript"></script>
<!-- Paper Dashboard Core javascript and methods for Demo purpose -->
		<script src="Assets/js/paper-dashboard.js"></script>
<!-- Paper Dashboard DEMO methods, don't include it in your project! -->
<!-- <script src="Assets/js/demo.js"></script>
<script src="Assets/js/jquery.sharrre.js"></script> -->
	</head>
<body>
	<div class="wrapper">
		<div class="sidebar" data-background-color="white" data-active-color="danger">
<!--
	Tip 1: you can change the color of the sidebar's background using: data-background-color="white | black"
	Tip 2: you can change the color of the active button using the data-active-color="primary | info | success | warning | danger"
-->
			<div class="sidebar-wrapper">
				<div class="logo">
					<a href="" class="simple-text">
						JOINT INVESTIGATION TEAM
					</a>
				</div>
				<ul class="nav">
							 <li>
								 <a href="dashboard.php">
									 <i class="ti-panel"></i>
									 <p>Dashboard</p>
								 </a>
							 </li>
							 <li class="active">
								 <a href="teamfrm.php">
									 <i class="ti-user"></i>
									 <p>กรอกข้อมูลทีมสอบสวนโรค</p>
								 </a>
							 </li>
					 <li>
								 <a href="iaplst.php">
									 <i class="ti-marker"></i>
									 <p>รายงานสถานการณ์ประจำวัน</p>
								 </a>
							 </li>
							 <li>
								 <a href="http://www.boeeoc.moph.go.th/eventbase/calendar/zone99/">
									 <i class="ti-notepad"></i>
									 <p>ปฏิทินแจ้งข่าวการระบาด</p>
								 </a>
							 </li>
							 <li>
								 <a href="teamlst.php">
									 <i class="ti-view-list-alt"></i>
									 <p>ตารางทีมที่ออกสอบสวน</p>
								 </a>
							 </li>
							 <li>
								 <a href="http://203.157.15.110/boe/software/downloadtab.php#tab5">
									 <i class="ti-clipboard"></i>
									 <p>แบบฟอร์มสอบสวนโรค</p>
								 </a>
							 </li>
							 <li>
								 <a href="maps.php">
									 <i class="ti-map"></i>
									 <p>Maps</p>
								 </a>
							 </li>
					<li class="active-pro">
							 </li>
						 </ul>
			</div>
		</div>
		<div class="main-panel">
			<nav class="navbar navbar-default">
				<div class="container-fluid">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar bar1"></span>
							<span class="icon-bar bar2"></span>
							<span class="icon-bar bar3"></span>
						</button>
						<a class="navbar-brand" href="#">ข้อมูลโรคและสถานที่ออกสอบสวนโรค</a>
					</div>
				<div class="collapse navbar-collapse">
					<ul class="nav navbar-nav navbar-right">
						<li>
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
								<i class="ti-panel"></i>
								<p>Stats</p>
							</a>
						</li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
								<i class="ti-bell"></i>
								<p class="notification">5</p>
								<p>Notifications</p>
								<b class="caret"></b>
							</a>
								<ul class="dropdown-menu">
									<li><a href="#">Notification 1</a></li>
									<li><a href="#">Notification 2</a></li>
									<li><a href="#">Notification 3</a></li>
									<li><a href="#">Notification 4</a></li>
									<li><a href="#">Another notification</a></li>
								</ul>
						</li>
						<li>
							<a href="logout.php">
								<i class="ti-power-off"></i>
								<p>logout</p>
							</a>
						</li>
					</ul>
				</div>
			</div>
		</nav>
		<div class="content">
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12 col-md-12">
						<div class="card">
							<div class="header">
							</div>
							<div class="content">
								<div class="card mt-5">
									<div class="card-header">
									</div>
									<div class="card-body">
										<?php if(!empty($message)): ?>
											<div class="alert alert-success">
												<?= $message; ?>
											</div>
										<?php endif; ?>
		<form name="register" role="form" action="personUD.php" method="post" enctype="multipart/form-data">
			<fieldset>
				<legend class="navbar-brand">แก้ไขรายชื่อสมาชิกทีม</legend>
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
						<div class="form-group required">
							<label for="title" class="control-label">ชื่อ :</label>
								<input type="hidden" name="id" value="<?php echo $person[0]['id'];?>">
								<input value="<?php echo $person[0]['firstname']; ?>" type="text" name="firstname" id="firstname" class="form-control">
						</div>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
						<div class="form-group required">
								<label for="title" class="control-label">นามสกุล :</label>
								<input value="<?php echo $person[0]['surname']; ?>" type="text" name="surname" id="surname" class="form-control">
						</div>
					</div>
				</div>
			</fieldset>
			<fieldset>
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
						<div class="form-group required">
							<label for="title" class="control-label">ตำแหน่ง :</label>
								<select class="form-control" name="position" id="position">
									<option value="<?php $position=$person[0]['position']; echo $position;?>"> <?php $position=$person[0]['position']; echo $arr_position[$position];?> </option>
									<option value="">--- โปรดเลือกตำแหน่ง ---</option>
									<option value="1">Supervisor</option>
									<option value="2">PI/Co-PI</option>
									<option value="3">Team Member</option>
								</select>
						</div>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
						<div class="form-group required">
							<label for="title" class="control-label">หน่วยงาน :</label>
								<select class="form-control" name="division" id="division">
									<option value="<?php $division=$person[0]['division']; echo $division;?>"> <?php $division=$person[0]['division']; echo $arr_division[$division];?> </option>
									<option value="">--- โปรดเลือกหน่วยงาน ---</option>
									<option value="1">สำนักสื่อสารความเสี่ยงและพัฒนาพฤติกรรมสุขภาพ</option>
									<option value="2">สำนักโรคติดต่อนำโดยแมลง</option>
									<option value="3">สำนักโรคติดต่ออุบัติใหม่</option>
									<option value="4">สถาบันเวชศาสตร์ป้องกันศึกษา</option>
									<option value="5">สำนักโรคไม่ติดต่อ</option>
									<option value="6">สถาบันราชประชาสมาสัย</option>
									<option value="7">สำนักวัณโรค</option>
									<option value="8">สำนักโรคจากการประกอบอาชีพและสิ่งแวดล้อม</option>
									<option value="9">กองโรคป้องกันด้วยวัคซีน</option>
									<option value="10">สำนักสำนักงานคณะกรรมการผู้ทรงคุณวุฒิ</option>
									<option value="11">สำนักโรคเอดส์ วัณโรค และโรคติดต่อทางเพศสัมพันธ</option>
									<option value="12">สำนักโรคติดต่อทั่วไป</option>
									<option value="13">สำนักความร่วมมือระหว่างประเทศ</option>
									<option value="14">สถาบันบำราศนราดูร</option>
									<option value="15">สำนักควบคุมการบริโภคยาสูบ</option>
									<option value="16">สำนักงานคณะกรรมการควบคุมเครื่องดื่มแอลกอฮอล์</option>
									<option value="17">สถาบันวิจัย จัดการความรู้ และมาตรฐานการควบคุมโรค</option>
								</select>
						</div>
					</div>
				</div>
			</fieldset>
			<fieldset>
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
						<div class="form-group">
							<a href="personeditfrm.php?id=<?php echo $person[0]['id']; ?> " class="btn btn-primary">BACK</i></a>
							<button type="submit" class="btn btn-danger">บันทึกข้อมูล</button>
							<span id="xhr-progress" style="display: none;"><img src="Assets/icons/ajax-loader.gif"> กำลังบันทึกข้อมูล โปรดรอ...</span>
						</div>
					</div>
				</div>
			</fieldset>
			</form>
		</div>
	</div>
</div>
			<footer class="footer">
				<div class="container-fluid">
					<div class="copyright pull-right">
						&copy; <script>document.write(new Date().getFullYear())</script></i> by <a href="http://203.157.15.110/boe/">CEI</a>
					</div>
				</div>
			</footer>
			</div>
		</div>
	</div>
</div>
<!--   Core JS Files   -->
<script src="https://code.jquery.com/jquery-1.12.4.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js" type="text/javascript"></script>
<!--  Checkbox, Radio & Switch Plugins -->
<script src="Assets/js/bootstrap-checkbox-radio.js"></script>
<!--  Charts Plugin -->
<script src="Assets/js/chartist.min.js"></script>
<!--  Notifications Plugin    -->
<script src="Assets/js/bootstrap-notify.js"></script>
<!--  Google Maps Plugin    -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script>
<!-- Paper Dashboard Core javascript and methods for Demo purpose -->
<script src="Assets/js/paper-dashboard.js"></script>
<!-- Paper Dashboard DEMO methods, don't include it in your project! -->
<script src="Assets/js/demo.js"></script>
<script src="Assets/js/jquery.sharrre.js"></script>
</html>
