<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<link rel="apple-touch-icon" sizes="76x76" href="Assets/img/apple-icon.png">
	<link rel="icon" type="image/png" sizes="96x96" href="Assets/img/favicon.png">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>JIT</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />

    <!-- Bootstrap core CSS     -->
    <link href="Assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="Assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Paper Dashboard core CSS    -->
    <link href="Assets/css/paper-dashboard.css" rel="stylesheet"/>

    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="Assets/css/demo.css" rel="stylesheet" />

    <!--  Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>
    <link href="Assets/css/themify-icons.css" rel="stylesheet">

    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">

</head>
<body>

<div class="wrapper">
	<div class="sidebar" data-background-color="white" data-active-color="danger">

    <!--
		Tip 1: you can change the color of the sidebar's background using: data-background-color="white | black"
		Tip 2: you can change the color of the active button using the data-active-color="primary | info | success | warning | danger"
	-->

    	<div class="sidebar-wrapper">
							<div class="logo">
	                <a href="" class="simple-text">
	Joint investigation team
	                </a>
	            </div>
                </a>


				<ul class="nav">
							 <li>
								 <a href="dashboard.php">
									 <i class="ti-panel"></i>
									 <p>Dashboard</p>
								 </a>
							 </li>
							 <li>
								 <a href="teamfrm.php">
									 <i class="ti-user"></i>
									 <p>กรอกข้อมูลทีมสอบสวนโรค</p>
								 </a>
							 </li>
					 <li>
								 <a href="iaplst.php">
									 <i class="ti-marker"></i>
									 <p>รายงานสถานการณ์ประจำวัน</p>
								 </a>
							 </li>
							 <li>
								 <a href="http://www.boeeoc.moph.go.th/eventbase/calendar/zone99/">
									 <i class="ti-notepad"></i>
									 <p>ปฏิทินแจ้งข่าวการระบาด</p>
								 </a>
							 </li>
							 <li class="active">
								 <a href="teamlst.php">
									 <i class="ti-view-list-alt"></i>
									 <p>ตารางทีมที่ออกสอบสวน</p>
								 </a>
							 </li>
							 <li>
								 <a href="http://203.157.15.110/boe/software/downloadtab.php#tab5">
									 <i class="ti-clipboard"></i>
									 <p>แบบฟอร์มสอบสวนโรค</p>
								 </a>
							 </li>
							 <li>
								 <a href="maps.php">
									 <i class="ti-map"></i>
									 <p>Maps</p>
								 </a>
							 </li>
					<li class="active-pro">
							 </li>
						 </ul>
    	</div>
    </div>

    <div class="main-panel">
		<nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar bar1"></span>
                        <span class="icon-bar bar2"></span>
                        <span class="icon-bar bar3"></span>
                    </button>
                    <a class="navbar-brand" href="#">Table List</a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="ti-panel"></i>
								<p>Stats</p>
                            </a>
                        </li>
                        <li class="dropdown">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="ti-bell"></i>
                                    <p class="notification">5</p>
									<p>Notifications</p>
									<b class="caret"></b>
                              </a>
                              <ul class="dropdown-menu">
                                <li><a href="#">Notification 1</a></li>
                                <li><a href="#">Notification 2</a></li>
                                <li><a href="#">Notification 3</a></li>
                                <li><a href="#">Notification 4</a></li>
                                <li><a href="#">Another notification</a></li>
                              </ul>
                        </li>
												<li>
													<a href="logout.php">
															<i class="ti-power-off"></i>
														<p>logout</p>
						                            </a>
						                        </li>
                    </ul>

                </div>
            </div>
        </nav>
