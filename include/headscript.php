<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="Assets/img/apple-icon.png">
  <link rel="icon" type="image/png" sizes="96x96" href="Assets/img/favicon.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

  <title>Joint investigation team</title>

  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
	<meta name="viewport" content="width=device-width" />

	<!--  Social tags      -->
	<meta name="keywords" content="bootstrap dashboard, creative tim, html dashboard, html css dashboard, web dashboard, paper design, bootstrap dashboard, bootstrap, css3 dashboard, bootstrap admin, paper bootstrap dashboard, frontend, responsive bootstrap dashboard">
	<meta name="description" content="Paper Dashboard is a beautiful Bootstrap Admin Panel for your next project.">
	<!-- Schema.org markup for Google+ -->
	<meta itemprop="name" content="Paper Dashboard by Creative Tim">
	<meta itemprop="description" content="Paper Dashboard is a beautiful Bootstrap Admin Panel for your next project.">
	<meta itemprop="image" content="http://s3.amazonaws.com/creativetim_bucket/products/43/original/opt_pd_thumbnail.jpg">
	<!-- Twitter Card data -->
	<meta name="twitter:card" content="summary_large_image">
	<meta name="twitter:site" content="@creativetim">
	<meta name="twitter:title" content="Paper Dashboard PRO by Creative Tim">
	<meta name="twitter:description" content="Paper Dashboard is a beautiful Bootstrap Admin Panel for your next project.">
	<meta name="twitter:creator" content="@creativetim">
	<meta name="twitter:image" content="http://s3.amazonaws.com/creativetim_bucket/products/43/original/opt_pd_thumbnail.jpg">
	<!-- Open Graph data -->
	<meta property="og:title" content="Paper Dashboard by Creative Tim" />
	<meta property="og:type" content="article" />
	<meta property="og:url" content="http://demos.creative-tim.com/paper-dashboard/dashboard.html" />
	<meta property="og:image" content="http://s3.amazonaws.com/creativetim_bucket/products/43/original/opt_pd_thumbnail.jpg"/>
	<meta property="og:description" content="Paper Dashboard is a beautiful Bootstrap Admin Panel for your next project." />
	<meta property="og:site_name" content="Creative Tim" />
	<!-- Bootstrap core CSS     -->
	<link href="Assets/css/bootstrap.min.css" rel="stylesheet" />
	<!-- Animation library for notifications   -->
	<link href="Assets/css/animate.min.css" rel="stylesheet"/>
	<!--  Paper Dashboard core CSS    -->
	<link href="Assets/css/paper-dashboard.css" rel="stylesheet"/>
	<!--  CSS for Demo Purpose, don't include it in your project     -->
	<!-- <link href="Assets/css/demo.css" rel="stylesheet" /> -->
	<!--  Fonts and icons     -->
	<link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
	<link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>
	<link href="Assets/css/themify-icons.css" rel="stylesheet">
	<script src="Assets/jquery/jquery.min.js" type="text/javascript"></script>
		<!-- Paper Dashboard Core javascript and methods for Demo purpose -->
	 <script src="Assets/js/paper-dashboard.js"></script>
	 <link rel="stylesheet"href="https://google-developers.appspot.com/_static/0d76052693/css/devsite-cyan.css">
	 <script src="https://google-developers.appspot.com/_static/0d76052693/js/prettify-bundle.js"></script>
	 <script src="https://google-developers.appspot.com/_static/0d76052693/js/jquery-bundle.js"></script>
	 <script src="//www.google.com/jsapi?key=AIzaSyCZfHRnq7tigC-COeQRmoa9Cxr0vbrK6xw"></script>
	 <script src="https://google-developers.appspot.com/_static/0d76052693/js/framebox.js"></script>
	<script src="Assets/jquery/jquery.min.js" type="text/javascript"></script>
	<script src="Assets/bootstrap/js/moment.js" type="text/javascript"></script>
	<script src="Assets/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="Assets/bootstrap/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
	<script src="Assets/jquery/jquery-ui.min.js" type="text/javascript"></script>
	<script src="Assets/bootstrap/js/validator.js" type="text/javascript"></script>

	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
</head>
