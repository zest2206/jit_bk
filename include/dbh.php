<?php
class dbh
{
	protected $dsn_db;
	protected $username;
	protected $password;
	protected $options;
	public $dbh;

	public function __construct() {
		require_once 'conf.php';
		$this->dsn_db = $dsn_db;
		$this->username = $username;
		$this->password = $password;
		$this->options = $options;
		$this->dbh = null;
		$this->connect();
	}

	public function connect() {
		try {
			$this->dbh = new PDO($this->dsn_db, $this->username, $this->password, $this->options);
			$this->dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			return $this->dbh;
		}
		catch (PDOException $e) {
			echo "Could not Connect: ".$e->getMessage();
			exit();
		}
	}

	public function __distruct() {
		$this->dbh = null;
		return true;
	}
}
?>
