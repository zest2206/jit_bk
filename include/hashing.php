<?php
namespace Hashing;
abstract class hash
{
	protected $hashStr;
	protected $saltSrt;
	protected $hashed;
	protected $salted;

	protected function __construct($hashStr=null, $saltStr=null) {
		$this->hashStr = $hashStr;
		$this->saltStr = $saltStr;
		$this->hashed = null;
		return true;
	}
	
	protected function setHashing() {
		$this->hashed = hash("sha512", $this->hashStr, false);
		return $this->hashed;
	}
	
	protected function setSalt() {
		$this->salted = hash("sha512", $this->saltStr, false);
		return $this->salted;
	}
}

class Hashed extends hash
{
	public $hashString;
	
	public function __construct($hashStr, $saltStr) {
		parent::__construct($hashStr, $saltStr);
	}
	
	public function getHashString() {
		$hash = hash::setHashing();
		$salt = hash::setSalt();
		$hashed = hash("sha512", $hash.$salt, false);
		return $hashed;
	}
}

?>