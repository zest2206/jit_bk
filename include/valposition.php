<?php
$arr_class_team = array(
'1'=>'ส่วนกลาง',
'2'=>'สคร.',
'3'=>'สสจ.',
'4'=>'สสอ.',
'5'=>'รพสต.',
'6'=>''
);
$arr_position = array(
'1'=>'Supervisor',
'2'=>'PI/Co-PI',
'3'=>'Team Member'
);
$arr_division = array(
'1'=>'สำนักสื่อสารความเสี่ยงและพัฒนาพฤติกรรมสุขภาพ',
'2'=>'สำนักโรคติดต่อนำโดยแมลง',
'3'=>'สำนักโรคติดต่ออุบัติใหม่',
'4'=>'สถาบันเวชศาสตร์ป้องกันศึกษา',
'5'=>'สำนักโรคไม่ติดต่อ',
'6'=>'สถาบันราชประชาสมาสัย',
'7'=>'สำนักวัณโรค',
'8'=>'สำนักโรคจากการประกอบอาชีพและสิ่งแวดล้อม',
'9'=>'กองโรคป้องกันด้วยวัคซีน',
'10'=>'สำนักสำนักงานคณะกรรมการผู้ทรงคุณวุฒิ',
'11'=>'สำนักโรคเอดส์ วัณโรค และโรคติดต่อทางเพศสัมพันธ์',
'12'=>'สำนักโรคติดต่อทั่วไป',
'13'=>'สำนักความร่วมมือระหว่างประเทศ',
'14'=>'สถาบันบำราศนราดูร',
'15'=>'สำนักควบคุมการบริโภคยาสูบ',
'16'=>'สำนักงานคณะกรรมการควบคุมเครื่องดื่มแอลกอฮอล์',
'17'=>'สถาบันวิจัย จัดการความรู้ และมาตรฐานการควบคุมโรค'
)
?>
