<?php
require_once 'dbh.php';
class querys extends dbh
{
	public $result;
	public $lastInsertId;
	public function __construct() {
		parent:: __construct();
	}

	public function getRegister() {
		try {
			$this->result = null;
			$sql = "SELECT * FROM tbl_response ORDER BY id ASC";
			$sth = $this->dbh->prepare($sql);
			$sth->execute();
			$this->result = $sth->fetchAll(PDO::FETCH_ASSOC);
			return $this->result;
		} catch (PDOException $e) {
			echo $e->getMessage();
		}
	}
	public function getPerson() {
		try {
			$this->result = null;
			$sql = "SELECT * FROM tbl_person ORDER BY Id_Team ASC";
			$sth = $this->dbh->prepare($sql);
			$sth->execute();
			$this->result = $sth->fetchAll(PDO::FETCH_ASSOC);
			return $this->result;
		} catch (PDOException $e) {
			echo $e->getMessage();
		}
	}


	public function getidcase($id=0) {
		try {
			$this->result = null;
			$sql = "SELECT * FROM tbl_response WHERE event_id=? ORDER BY id DESC";
			// echo $sql;
			$sth = $this->dbh->prepare($sql);
			$sth->execute(array($id));
			$this->result = $sth->fetchAll(PDO::FETCH_ASSOC);
			if (count($this->result) > 0) {
				return $this->result;
			} else {
				return false;
			}
		} catch (PDOException $e) {
			echo $e->getMessage();
		}
	}

	public function geteventid($id=0) {
		try {
			$this->resulte = null;
			$sql = "SELECT * FROM tbl_response WHERE event_id=? ORDER BY id DESC";
			// echo $sql;
			$sth = $this->dbh->prepare($sql);
			$sth->execute(array($id));
			$this->resulte = $sth->fetchAll(PDO::FETCH_ASSOC);
			if (count($this->resulte) > 0) {
				return $this->resulte;
			} else {
				return false;
			}
		} catch (PDOException $e) {
			echo $e->getMessage();
		}
	}

	public function getidteam($id=0) {
		try {
			$this->result = null;
			$sql = "SELECT * FROM tbl_response WHERE Id_Team=? ORDER BY id DESC";
			// echo $sql;
			$sth = $this->dbh->prepare($sql);
			$sth->execute(array($id));
			$this->result = $sth->fetchAll(PDO::FETCH_ASSOC);
			if (count($this->result) > 0) {
				return $this->result;
			} else {
				return false;
			}
		} catch (PDOException $e) {
			echo $e->getMessage();
		}
	}

	public function getidperson($id=0) {
		try {
			$this->result = null;
			$sql = "SELECT * FROM tbl_person WHERE Id_Team=? ORDER BY Id_Team DESC";
			// echo $sql;
			$sth = $this->dbh->prepare($sql);
			$sth->execute(array($id));
			$this->result = $sth->fetchAll(PDO::FETCH_ASSOC);
			if (count($this->result) > 0) {
				return $this->result;
			} else {
				return false;
			}
		} catch (PDOException $e) {
			echo $e->getMessage();
		}
	}
	public function getprovince() {
		try {
			$this->result = null;
			$sql = "SELECT province_name FROM tbl_provinces ORDER BY province_id ASC";
			$sth = $this->dbh->prepare($sql);
			$sth->execute();
			$this->result = $sth->fetchAll(PDO::FETCH_ASSOC);
			return $this->result;
		} catch (PDOException $e) {
			echo $e->getMessage();
		}
	}
	public function getProvinceByID($prov_code=null) {
		try {
			$this->result = null;
			if ($prov_code != null) {
				$sql = "SELECT * FROM tbl_provinces WHERE province_id=?";
				$sth = $this->dbh->prepare($sql);
				$sth->execute(array($prov_code));
				$sth->execute();
				$this->result = $sth->fetchAll(PDO::FETCH_ASSOC);
				return $this->result;
			}
		} catch (PDOException $e) {
			echo $e->getMessage();
		}
	}
	public function getDataFromTable($table=null, $fields=array(), $condition=array(), $conditionVal=array()) {
		try {
			$this->result = null;
			$sql = "SELECT ";
			if (sizeof($fields) <= 0) {
				$sql .= "*";
			} else {
				$set = null;
				foreach($fields as $val) {
					if (is_null($set)) {
						$set = "";
					} else {
						$set = $set.", ";
					}
					$set = $set.$val;
				}
				$sql .= $set;
			}
			$sql .= " FROM ".$table." ";
			
			if (sizeof($condition > 0)) {
				foreach ($condition as $val) {
					$sql .= $val." ";
				}
			}
			if (sizeof($conditionVal > 0)) {
				$sth = $this->dbh->prepare($sql);
				$sth->execute($conditionVal);
			} else {
				$sth = $this->dbh->prepare($sql);
				$sth->execute();
			}
			$this->result = $sth->fetchAll(PDO::FETCH_ASSOC);
			return $this->result;
		} catch (PDOException $e) {
			echo $e->getMessage();
		}
	}
	public  function Update($nameTable, $where, $data){
		try {
			$this->result = null;
			while($element = each($data)){
								$update[$element["key"]]="`".$element["key"]."`='".$element["value"]."'";
						}
						while($element2 = each($where)){
								$where[$element2["key"]]=$element2["key"]."'".$element2["value"]."'";
						}
			$sql = "UPDATE `".$nameTable."` SET ".implode(",",$update)." WHERE ".implode(" and ",$where);
			 // echo $sql;
			 //  exit;
			$sth = $this->dbh->prepare($sql);
			$sth->execute();
			return true;
		} catch (PDOException $e) {
			echo $e->getMessage();
		}
	     }
	}
?>
