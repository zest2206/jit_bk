<?php
 // require_once 'include/valUser.php';
 require_once 'include/header.php';
 require_once 'include/querys.php';
 require_once 'include/valdisease.php';
 require_once 'include/valprovince.php';
 require_once 'include/valdistrict.php';
 require_once 'include/valamphures.php';
  require_once 'include/vallocation.php';


 $query = new querys();


 // $sql = "SELECT * FROM tbl_groupdisease ORDER BY GROUPDISEASE_ID ASC";
 // $stmt = $dbh_db->prepare($sql);
 // $stmt->execute();
 // $ds_group=$stmt->fetchAll(PDO::FETCH_ASSOC);

 ?>
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">ตารางกรอกสรุปสถานการณ์ประจำวันรายทีม</h4>
                            </div>
                            <div class="content table-responsive table-full-width">
                              <table id="example" class="hover" cellspacing="0" width="100%">
                                <thead>
                                  <tr>
									<th>รหัสเคส</th>
                                    <th>รหัสทีม</th>
                                    <!-- <th>รายชื่อสมาชิกทีม</th> -->
                                    <th>สถานที่</th>
                                    <th>วันที่ออกสอบสวน</th>
                                    <th>วันที่กลับ</th>
                                    <th>กลุ่มโรค-โรค</th>
									<th>ระดับทีมสอบสวน</th>
									<th>ตรวจสอบประวัติรายงานสถานการณ์ประจำวัน</th>
									<th>รายงานสถานการณ์ประจำวัน</th>
                                  </tr>
                                </thead>
                                <tfoot></tfoot>
                                <tbody>
                                <?php
                            //     $result = $query->getidcase($_GET['event_id']);
							// 	if ($result > 0 && $result != false) {
   							// 		var_dump($result);
							// 		} else {
   							// echo "Have a good night!";
							// }
		 // var_dump($evi);
		 // exit;
									 $result = $query->getidcase($_GET['event_id']);
									 if ($result > 0 && $result != false) {
										 foreach ($result as $key => $v) {
				//foreach ($value as $key => $v) {
                                		  echo "<tr>\n";
										  	echo "<td>".$v['event_id']."</td>\n";
		                                    echo "<td>".$v['Id_Team']."</td>\n";
                                		    // echo "<td>"."<button type=\"button\" class=\"btn btn-info btn-sm\" data-toggle=\"modal\" data-target=\"#myModal\">รายชื่อสมาชิกทึม</i></button>\n";
		                                    // echo "<td>"."<a href=\"personlst.php?Id_Team=".$v['Id_Team']."\" title=\"ดูข้อมูล\" class=\"btn btn-xs btn-info\"><i class=\"fa fa-eye\" aria-hidden=\"true\"></i></a>\n";
                                		    echo "<td>".$amphures[$v['amphur_name']]." ".$province[$v['province_name']]."</td>\n";
		                                    echo "<td>".$v['day_travel']." ".$month[$v['month_travel']]." ".$v['year_travel']."</td>\n";
                                		    echo "<td>".$v['day_return']." ".$month[$v['month_return']]." ".$v['year_return']."</td>\n";
		                                    echo "<td>".$groupdisease[$v['groupdisease_name']]." "," ".$disease[$v['disease_name']]."</td>\n";
											echo "<td>"."<a href=\"locationedit.php?Id_Team=".$v['Id_Team']."\" title=\"เพิ่มสถานที่ออกสอบสวนโรค\" class=\"btn btn-xs btn-info\"><i class=\"fa fa-globe\" aria-hidden=\"true\"></i></a>\n";
		                                    echo "</td>\n";
											echo "<td>\n";
											echo "<a href=\"iap.php?event_id=".$v['event_id']."\" title=\"ตรวจสอบประวัติรายงานสถานการณ์ประจำวัน\" class=\"btn btn-xs btn-info\"><i class=\"fa fa-list-ol\" aria-hidden=\"true\"></i>ตรวจสอบประวัติรายงานสถานการณ์ประจำวัน</a>\n";
											echo "</td>\n";
											echo "<td>\n";
											echo "<a href=\"iap.php?event_id=".$v['event_id']."&Id_Team=".$v['Id_Team']."\" title=\"ดูข้อมูล\" class=\"btn btn-xs btn-success\"><i class=\"fa fa-plus\" aria-hidden=\"true\"></i>เพิ่มสถานการณ์ประจำวัน</a>\n";
                                		    // echo "<a href=\"teameditfrm.php?Id_Team=".$v['Id_Team']."&Id_Team=".$v['Id_Team']."\" title=\"แก้ไขข้อมูล\" class=\"btn btn-xs btn-warning\"><i class=\"fa fa-pencil\" aria-hidden=\"true\"></i></a>\n";
		                                    echo "</td>\n";
                                		  	echo "</tr>\n";
			}

                                } else {
			echo "<tr><td>ยังไม่มีข้อมูล</td></tr>";
		}
                                ?>
                                </tbody>
                              </table>
                            </div>
							<a href="iaplst.php" class="btn btn-success" role="button" >กลับ</a>
                        </div>
                    </div>

                    <footer class="footer">
                        <div class="container-fluid">
            				<div class="copyright pull-right">
                                &copy; <script>document.write(new Date().getFullYear())</script></i> by <a href="http://203.157.15.110/boe/">CEI</a>
                            </div>
                        </div>
                    </footer>

    </div>
</div>

    </div>
</div>

</body>
<script src="Assets/js/jquery-1.10.2.js" type="text/javascript"></script>
<script src="Assets/js/bootstrap.min.js" type="text/javascript"></script>
<script>
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>
    <!--   Core JS Files   -->
    <script src="https://code.jquery.com/jquery-1.12.4.js" type="text/javascript"></script>
	<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js" type="text/javascript"></script>

	<!--  Checkbox, Radio & Switch Plugins -->
	<script src="Assets/js/bootstrap-checkbox-radio.js"></script>

	<!--  Charts Plugin -->
	<script src="Assets/js/chartist.min.js"></script>

    <!--  Notifications Plugin    -->
    <script src="Assets/js/bootstrap-notify.js"></script>

    <!--  Google Maps Plugin    -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script>

    <!-- Paper Dashboard Core javascript and methods for Demo purpose -->
	<script src="Assets/js/paper-dashboard.js"></script>

	<!-- Paper Dashboard DEMO methods, don't include it in your project! -->
	<script src="Assets/js/demo.js"></script>
    <script src="Assets/js/jquery.sharrre.js"></script>

</html>
