<?php
 require_once 'include/valdisease.php';
 require_once 'mysql_connect.php';
 require_once 'include/conf.php';
 require_once 'include/querys.php';
 require_once 'include/valdisease.php';


 $query = "SELECT
tbl_response.id,
tbl_response.Id_Team,
tbl_response.class_team_other,
tbl_response.class_team,
tbl_response.province_name,
tbl_response.amphur_name,
tbl_response.district_name,
tbl_response.groupdisease_name,
tbl_response.groupdisease_other_name,
tbl_response.disease_name,
tbl_response.disease_other_name,
tbl_response.week_travel,
tbl_response.day_travel,
tbl_response.month_travel,
tbl_response.year_travel,
tbl_response.week_return,
tbl_response.day_return,
tbl_response.month_return,
tbl_response.year_return,
tbl_response.other,
tbl_response.date_entry,
tbl_location.lat,
tbl_location.lng
FROM
tbl_response
LEFT JOIN tbl_location ON tbl_response.Id_Team = tbl_location.Id_Team ;  ";
// echo $query;
// exit;
 $locations = $conn->query($query);
 // // var_dump($locations);
 // exit;
 ?>
 <!doctype html>
 <html lang="en">
 <head>
 	<meta charset="utf-8" />
 	<link rel="apple-touch-icon" sizes="76x76" href="Assets/img/apple-icon.png">
 	<link rel="icon" type="image/png" sizes="96x96" href="Assets/img/favicon.png">
 	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

 	<title>Joint investigation team</title>

 	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
     <meta name="viewport" content="width=device-width" />

     <!--  Social tags      -->
     <meta name="keywords" content="bootstrap dashboard, creative tim, html dashboard, html css dashboard, web dashboard, paper design, bootstrap dashboard, bootstrap, css3 dashboard, bootstrap admin, paper bootstrap dashboard, frontend, responsive bootstrap dashboard">

     <meta name="description" content="Paper Dashboard is a beautiful Bootstrap Admin Panel for your next project.">

     <!-- Schema.org markup for Google+ -->
     <meta itemprop="name" content="Paper Dashboard by Creative Tim">
     <meta itemprop="description" content="Paper Dashboard is a beautiful Bootstrap Admin Panel for your next project.">
     <meta itemprop="image" content="http://s3.amazonaws.com/creativetim_bucket/products/43/original/opt_pd_thumbnail.jpg">

     <!-- Twitter Card data -->
     <meta name="twitter:card" content="summary_large_image">
     <meta name="twitter:site" content="@creativetim">
     <meta name="twitter:title" content="Paper Dashboard PRO by Creative Tim">
     <meta name="twitter:description" content="Paper Dashboard is a beautiful Bootstrap Admin Panel for your next project.">
     <meta name="twitter:creator" content="@creativetim">
     <meta name="twitter:image" content="http://s3.amazonaws.com/creativetim_bucket/products/43/original/opt_pd_thumbnail.jpg">

     <!-- Open Graph data -->
     <meta property="og:title" content="Paper Dashboard by Creative Tim" />
     <meta property="og:type" content="article" />
     <meta property="og:url" content="http://demos.creative-tim.com/paper-dashboard/dashboard.html" />
     <meta property="og:image" content="http://s3.amazonaws.com/creativetim_bucket/products/43/original/opt_pd_thumbnail.jpg"/>
     <meta property="og:description" content="Paper Dashboard is a beautiful Bootstrap Admin Panel for your next project." />
     <meta property="og:site_name" content="Creative Tim" />


     <!-- Bootstrap core CSS     -->
     <link href="Assets/css/bootstrap.min.css" rel="stylesheet" />

     <!-- Animation library for notifications   -->
     <link href="Assets/css/animate.min.css" rel="stylesheet"/>

     <!--  Paper Dashboard core CSS    -->
     <link href="Assets/css/paper-dashboard.css" rel="stylesheet"/>


     <!--  CSS for Demo Purpose, don't include it in your project     -->
     <!-- <link href="Assets/css/demo.css" rel="stylesheet" /> -->


     <!--  Fonts and icons     -->
     <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
     <link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>
     <link href="Assets/css/themify-icons.css" rel="stylesheet">
    <link rel="stylesheet"href="https://google-developers.appspot.com/_static/0d76052693/css/devsite-cyan.css">
    <script src="https://google-developers.appspot.com/_static/0d76052693/js/prettify-bundle.js"></script>
    <script src="https://google-developers.appspot.com/_static/0d76052693/js/jquery-bundle.js"></script>
    <script src="//www.google.com/jsapi?key=AIzaSyCZfHRnq7tigC-COeQRmoa9Cxr0vbrK6xw"></script>
    <script src="https://google-developers.appspot.com/_static/0d76052693/js/framebox.js"></script>

			<script src="Assets/js/paper-dashboard.js"></script>
    </head>
    <body>

    <div class="wrapper">
        <div class="sidebar" data-background-color="white" data-active-color="danger">

        <!--
    		Tip 1: you can change the color of the sidebar's background using: data-background-color="white | black"
    		Tip 2: you can change the color of the active button using the data-active-color="primary | info | success | warning | danger"
    	-->

        	<div class="sidebar-wrapper">
                <div class="logo">
                    <a href="" class="simple-text">
    Joint investigation team
                    </a>
                </div>

				<ul class="nav">
							 <li>
								 <a href="dashboard.php">
									 <i class="ti-panel"></i>
									 <p>Dashboard</p>
								 </a>
							 </li>
							 <li>
								 <a href="teamfrm.php">
									 <i class="ti-user"></i>
									 <p>กรอกข้อมูลทีมสอบสวนโรค</p>
								 </a>
							 </li>
					 <li>
								 <a href="iaplst.php">
									 <i class="ti-marker"></i>
									 <p>รายงานสถานการณ์ประจำวัน</p>
								 </a>
							 </li>
							 <li>
								 <a href="http://www.boeeoc.moph.go.th/eventbase/calendar/zone99/">
									 <i class="ti-notepad"></i>
									 <p>ปฏิทินแจ้งข่าวการระบาด</p>
								 </a>
							 </li>
							 <li class="active">
								 <a href="teamlst.php">
									 <i class="ti-view-list-alt"></i>
									 <p>ตารางทีมที่ออกสอบสวน</p>
								 </a>
							 </li>
							 <li>
								 <a href="http://203.157.15.110/boe/software/downloadtab.php#tab5">
									 <i class="ti-clipboard"></i>
									 <p>แบบฟอร์มสอบสวนโรค</p>
								 </a>
							 </li>
							 <li>
								 <a href="maps.php">
									 <i class="ti-map"></i>
									 <p>Maps</p>
								 </a>
							 </li>
					<li class="active-pro">
							 </li>
						 </ul>
        	</div>
        </div>

        <div class="main-panel">
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar bar1"></span>
                            <span class="icon-bar bar2"></span>
                            <span class="icon-bar bar3"></span>
                        </button>
                        <a class="navbar-brand" href="#">Maps</a>
                    </div>
                    <div class="collapse navbar-collapse">
                        <ul class="nav navbar-nav navbar-right">
                            <li>
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="ti-panel"></i>
    								<p>Stats</p>
                                </a>
                            </li>
                            <li class="dropdown">
                                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                        <i class="ti-bell"></i>
                                        <p class="notification">5</p>
    									<p>Notifications</p>
    									<b class="caret"></b>
                                  </a>
                                  <ul class="dropdown-menu">
                                    <li><a href="#">Notification 1</a></li>
                                    <li><a href="#">Notification 2</a></li>
                                    <li><a href="#">Notification 3</a></li>
                                    <li><a href="#">Notification 4</a></li>
                                    <li><a href="#">Another notification</a></li>
                                  </ul>
                            </li>
                            <li>
                							<a href="logout.php">
                									<i class="ti-power-off"></i>
                								<p>logout</p>
                                            </a>
                                        </li>
                        </ul>

                    </div>
                </div>
            </nav>

            <div class="content">
                   <div class="container-fluid">
                       <div class="row">
                           <div class="col-md-12">
                               <div class="card">
                                   <div class="content">
<!-- Maps -->
          <fieldset>
            <div class="row">
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <div class="form-group required">
                  <legend>แผนที่แสดงตำแหน่งทีมสอบสวนโรค</legend>
                    <div id="map" style="width:100%;height:700px"></div>
                        <script>
                          function myMap() {
                                  var myCenter = new google.maps.LatLng(13.616413, 101.323434);
                                  var mapCanvas = document.getElementById("map");
                                  var mapOptions = {center: myCenter, zoom: 6};
                                  var map = new google.maps.Map(mapCanvas, mapOptions);
                                  <?php while ($row=$locations->FETCH_ASSOC()) { ?>
                                        var location = new google.maps.LatLng(<?php echo $row['lat']; ?>, <?php echo $row['lng']; ?>);
                                        var marker = new google.maps.Marker({position: location});
                                        marker.setMap(map);
                                        google.maps.event.addListener(marker,'click',function() {
                                        var infowindow = new google.maps.InfoWindow({
                                            content:
                                            "<b>รหัสทีมที่ออกสอบสวนโรค : <?php echo $row['Id_Team']; ?></b></br>"+
                                            "อำเภอ : <?php echo $amphures[$row['amphur_name']]; ?> "+
                                            "จังหวัด : <?php echo $province[$row['province_name']]; ?></br>"+
                                            "กลุ่มโรค : <?php echo $groupdisease[$row['groupdisease_name']]; ?></br>"+
                                            "โรค : <?php echo $disease[$row['disease_name']]; ?> </br>"+
                                            "รายละเอียด : <a href=jitteamview.php?Id_Team=<?php echo $row['Id_Team'] ?>>test</a>"
                                          });
                                        infowindow.open(map,marker);
                                        });
                                <?php } ?>
                                }
                        </script>
                        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCAP4jkz9nPhjRqMskknIor2prtqs8dcK4&callback=myMap"></script>
                  </div>
                </div>
  <!-- end maps-->
