<?php
require_once 'include/vallocation.php';
require_once 'include/querys.php';
$register = new querys();


$regis = $register->getDataFromTable(
		'tbl_response', array(), array('WHERE Id_Team=?', 'ORDER BY Id_Team ASC'), array($_GET['Id_Team'])
		);
$resive = $register->getDataFromTable(
	'tbl_person', array(), array('WHERE Id_Team=?', 'ORDER BY Id_Team ASC'), array($_GET['Id_Team'])
			 );
$resivelocation = $register->getDataFromTable(
	'tbl_location', array(), array('WHERE Id_Team=?', 'ORDER BY Id_Team ASC'), array($_GET['Id_Team'])
			 );

 ?>
 <!doctype html>
 <html lang="en">
 <head>
 	<meta charset="utf-8" />
 	<link rel="apple-touch-icon" sizes="76x76" href="Assets/img/apple-icon.png">
 	<link rel="icon" type="image/png" sizes="96x96" href="Assets/img/favicon.png">
 	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

 	<title>Joint investigation team</title>

 	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
     <meta name="viewport" content="width=device-width" />

     <!--  Social tags      -->
     <meta name="keywords" content="bootstrap dashboard, creative tim, html dashboard, html css dashboard, web dashboard, paper design, bootstrap dashboard, bootstrap, css3 dashboard, bootstrap admin, paper bootstrap dashboard, frontend, responsive bootstrap dashboard">

     <meta name="description" content="Paper Dashboard is a beautiful Bootstrap Admin Panel for your next project.">

     <!-- Schema.org markup for Google+ -->
     <meta itemprop="name" content="Paper Dashboard by Creative Tim">
     <meta itemprop="description" content="Paper Dashboard is a beautiful Bootstrap Admin Panel for your next project.">
     <meta itemprop="image" content="http://s3.amazonaws.com/creativetim_bucket/products/43/original/opt_pd_thumbnail.jpg">

     <!-- Twitter Card data -->
     <meta name="twitter:card" content="summary_large_image">
     <meta name="twitter:site" content="@creativetim">
     <meta name="twitter:title" content="Paper Dashboard PRO by Creative Tim">
     <meta name="twitter:description" content="Paper Dashboard is a beautiful Bootstrap Admin Panel for your next project.">
     <meta name="twitter:creator" content="@creativetim">
     <meta name="twitter:image" content="http://s3.amazonaws.com/creativetim_bucket/products/43/original/opt_pd_thumbnail.jpg">

     <!-- Open Graph data -->
     <meta property="og:title" content="Paper Dashboard by Creative Tim" />
     <meta property="og:type" content="article" />
     <meta property="og:url" content="http://demos.creative-tim.com/paper-dashboard/dashboard.html" />
     <meta property="og:image" content="http://s3.amazonaws.com/creativetim_bucket/products/43/original/opt_pd_thumbnail.jpg"/>
     <meta property="og:description" content="Paper Dashboard is a beautiful Bootstrap Admin Panel for your next project." />
     <meta property="og:site_name" content="Creative Tim" />
     <!-- Bootstrap core CSS     -->
     <link href="Assets/css/bootstrap.min.css" rel="stylesheet" />
     <!-- Animation library for notifications   -->
     <link href="Assets/css/animate.min.css" rel="stylesheet"/>
     <!--  Paper Dashboard core CSS    -->
     <link href="Assets/css/paper-dashboard.css" rel="stylesheet"/>
     <!--  CSS for Demo Purpose, don't include it in your project     -->
     <!-- <link href="Assets/css/demo.css" rel="stylesheet" /> -->
     <!--  Fonts and icons     -->
     <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
     <link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>
     <link href="Assets/css/themify-icons.css" rel="stylesheet">
 	<script src="Assets/jquery/jquery.min.js" type="text/javascript"></script>
 		<!-- Paper Dashboard Core javascript and methods for Demo purpose -->
 	<script src="Assets/js/paper-dashboard.js"></script>
 			<!-- Paper Dashboard DEMO methods, don't include it in your project! -->
 			<!-- <script src="Assets/js/demo.js"></script>
 			<script src="Assets/js/jquery.sharrre.js"></script> -->
			<!-- นำเข้า Select2 CSS -->
			<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/css/select2.min.css" rel="stylesheet" />
 </head>
 <body>
 <div class="wrapper">
 	<div class="sidebar" data-background-color="white" data-active-color="danger">

     <!--
 		Tip 1: you can change the color of the sidebar's background using: data-background-color="white | black"
 		Tip 2: you can change the color of the active button using the data-active-color="primary | info | success | warning | danger"
 	-->

     	<div class="sidebar-wrapper">
             <div class="logo">
                 <a href="" class="simple-text">
                     JOINT INVESTIGATION TEAM
                 </a>
             </div>

			 <ul class="nav">
			 			 <li>
			 				 <a href="dashboard.php">
			 					 <i class="ti-panel"></i>
			 					 <p>Dashboard</p>
			 				 </a>
			 			 </li>
			 			 <li class="active">
			 				 <a href="teamfrm.php">
			 					 <i class="ti-user"></i>
			 					 <p>กรอกข้อมูลทีมสอบสวนโรค</p>
			 				 </a>
			 			 </li>
			 	 <li>
			 				 <a href="iaplst.php">
			 					 <i class="ti-marker"></i>
			 					 <p>รายงานสถานการณ์ประจำวัน</p>
			 				 </a>
			 			 </li>
			 			 <li>
			 				 <a href="http://www.boeeoc.moph.go.th/eventbase/calendar/zone99/">
			 					 <i class="ti-notepad"></i>
			 					 <p>ปฏิทินแจ้งข่าวการระบาด</p>
			 				 </a>
			 			 </li>
			 			 <li>
			 				 <a href="teamlst.php">
			 					 <i class="ti-view-list-alt"></i>
			 					 <p>ตารางทีมที่ออกสอบสวน</p>
			 				 </a>
			 			 </li>
			 			 <li>
			 				 <a href="http://203.157.15.110/boe/software/downloadtab.php#tab5">
			 					 <i class="ti-clipboard"></i>
			 					 <p>แบบฟอร์มสอบสวนโรค</p>
			 				 </a>
			 			 </li>
			 			 <li>
			 				 <a href="maps.php">
			 					 <i class="ti-map"></i>
			 					 <p>Maps</p>
			 				 </a>
			 			 </li>
			 	<li class="active-pro">
			 			 </li>
			 		 </ul>
     	</div>
     </div>

     <div class="main-panel">
 		<nav class="navbar navbar-default">
             <div class="container-fluid">
                 <div class="navbar-header">
                     <button type="button" class="navbar-toggle">
                         <span class="sr-only">Toggle navigation</span>
                         <span class="icon-bar bar1"></span>
                         <span class="icon-bar bar2"></span>
                         <span class="icon-bar bar3"></span>
                     </button>
                     <a class="navbar-brand" href="#">ข้อมูลโรคและสถานที่ออกสอบสวนโรค</a>
                 </div>
                 <div class="collapse navbar-collapse">
                     <ul class="nav navbar-nav navbar-right">
                         <li>
                             <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                 <i class="ti-panel"></i>
 								<p>Stats</p>
                             </a>
                         </li>
                         <li class="dropdown">
                               <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                     <i class="ti-bell"></i>
                                     <p class="notification">5</p>
 									<p>Notifications</p>
 									<b class="caret"></b>
                               </a>
                               <ul class="dropdown-menu">
                                 <li><a href="#">Notification 1</a></li>
                                 <li><a href="#">Notification 2</a></li>
                                 <li><a href="#">Notification 3</a></li>
                                 <li><a href="#">Notification 4</a></li>
                                 <li><a href="#">Another notification</a></li>
                               </ul>
                         </li>
 						<li>
 							<a href="logout.php">
 									<i class="ti-power-off"></i>
 								<p>logout</p>
                             </a>
                         </li>
                     </ul>

                 </div>
             </div>
         </nav>


         <div class="content">
             <div class="container-fluid">
                 <div class="row">

                     <div class="col-lg-12 col-md-12">
                         <div class="card">
                             <div class="header">
                             </div>
                             <div class="content">
            <form name="register" role="form" action="teamfrmUD.php" method="post" enctype="multipart/form-data">
      <!--	<form name="register" role="form" action="addproductPS.php" method="post" enctype="multipart/form-data"> -->
      <fieldset>
        <legend>รหัสทีมสอบสวน</legend>
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
            <div class="form-group required">
              <label for="title" class="control-label">รหัสโรคที่ออกสอบสวน :</label>
              <input type="text" name="event_id" id="event_id" class="form-control" placeholder="รหัสโรคที่ออกสอบสวน" data-error="" value="<?php echo $regis[0]['event_id']; ?>" required readonly>
              <div class="help-block with-errors"></div>
            </div>
          </div>
					<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
						<div class="form-group required">
							<label for="title" class="control-label">รหัสทีมสอบสวน :</label>
							<input type="text" name="Id_Team" id="Id_Team" class="form-control" placeholder="รหัสทีมสอบสวน" data-error="" value="<?php echo $regis[0]['Id_Team']; ?>" required readonly>
							<div class="help-block with-errors"></div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
													<div class="form-group">
														<label for="title" class="control-label">เลขเลขฎีกาเบิกจ่ายเงิน (หากไม่ทราบสามารถแก้ไขภายหลังได้) :</label>
						<input type="text" name="finance_no" id="finance_no" value="<?php echo $regis[0]['finance_no']; ?>"  class="form-control" placeholder="เลขเลขฎีกาเบิกจ่ายเงิน" data-error="" >

													</div>
												</div>
      </div>
      </fieldset>
			<fieldset>
				<legend>ระดับทีมสอบสวน</legend>
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
						<div class="form-group required">
							<label for="title"  class="control-label">ระดับทีมสอบสวน :</label>
							<select class="form-control" id="ddlPassport" name="class_team"  title="ตำแหน่งในทีม" value="<?php echo $regis[0]['class_team']; ?>" required>
												<option value="1">ส่วนกลาง</option>
												<option value="2">สคร.</option>
												<option value="3">สสจ.</option>
												<option value="4">สสอ.</option>
												<option value="5">รพสต.</option>
												<option value="6">อื่นๆ</option>
												</select>
							<div class="help-block with-errors"></div>
					</div>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
					<label for="title" class="control-label">ระดับทีมอื่นๆ :</label>
						 <input type="text" name="class_team_other" id="class_team_other"  value="<?php echo $regis[0]['class_team_other']; ?>" class="form-control" placeholder="ระดับทีมอื่นๆ">
					<div class="help-block with-errors"></div>
			</div>
			</fieldset>
      <fieldset>
        <legend>สถานที่ออกปฏิบัติงาน</legend>
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
            <div class="form-group required">
              <label for="title" class="control-label">จังหวัด :</label>
							<select class="form-control selectlo-single" name="province_name" id="province">
								<option value="<?php $province=$regis[0]['province_name']; echo $province;?>"> <?php $province=$regis[0]['province_name']; echo $arr_province[$province];?> </option>
						 </select>
            </div>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
            <div class="form-group required">
              <label for="title" class="control-label">อำเภอ/เขต :</label>
							<select class="form-control selectlo-single" name="amphur_name" id="amphur">
								<option value="<?php $amphur=$regis[0]['amphur_name']; echo $amphur;?>"> <?php $amphur=$regis[0]['amphur_name']; echo $arr_amphur[$amphur];?> </option>
							</select>
              <div class="help-block with-errors"></div>
            </div>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
            <div class="form-group required">
              <label for="title" class="control-label">ตำบล/แขวง :</label>
							<select class="form-control selectlo-single" name="district_name" id="district">
								<option value="<?php $district=$regis[0]['district_name']; echo $district; ?>"><?php $district=$regis[0]['district_name']; echo $arr_district[$district]; ?></option>
							</select>
              <div class="help-block with-errors"></div>
            </div>
          </div>
        </div>
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
					<div class="form-group required">
						<label for="title" class="control-label">Latitude :</label>
							 <input type="text" name="lat" id="lat" class="form-control" placeholder="" value="<?php echo $resivelocation[0]['lat']; ?>">
					 </select>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
					<div class="form-group required">
						<label for="title" class="control-label">Longitude :</label>
						<input type="text" name="lng" id="lng" class="form-control" placeholder="" value="<?php echo $resivelocation[0]['lng']; ?>" >
						<div class="help-block with-errors"></div>
					</div>
				</div>
				</div>
    </fieldset>
    <fieldset>
        <legend>กลุ่มโรคและภัยที่ออกปฏิบัติงาน</legend>
            <div class="row">
              <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
              <div class="form-group required">
                <label for="title" class="control-label">กลุ่มโรค :</label>
                <select class="form-control select2-single" name="groupdisease_name" id="groupdisease" >
                 <option value="<?php	$groupdisease=$regis[0]['groupdisease_name']; echo $groupdisease;?>"><?php	$groupdisease=$regis[0]['groupdisease_name']; echo $arr_groupdisease[$groupdisease];?></option>
               </select>
              </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
              <div class="form-group required">
                <label for="title" class="control-label">กลุ่มโรคอื่นๆ :</label>
                   <input type="text" name="groupdisease_other_name" id="groupdisease_other_name" class="form-control" placeholder="" value="<?php echo $regis[0]['groupdisease_other_name']; ?>">
                <div class="help-block with-errors"></div>
              </div>
            </div>
            </div>
          </fieldset>
          <fieldset>
              <legend>โรคและภัยที่ออกปฏิบัติงาน</legend>
                  <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                    <div class="form-group required">
                      <label for="title" class="control-label">โรค :</label>
                      <select class="form-control select2-single" name="disease_name" id="disease" >
                        <option value="<?php $disease=$regis[0]['disease_name']; echo $disease;?>"><?php $disease=$regis[0]['disease_name']; echo $arr_disease[$disease];?></option>
                      </select>
                    </div>
                  </div>
                  <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                    <div class="form-group required">
                      <label for="title" class="control-label">โรคอื่นๆ :</label>
                         <input type="text" name="disease_other_name" id="disease_other_name" class="form-control" placeholder="" value="<?php echo $regis[0]['disease_other_name']; ?>">
                      <div class="help-block with-errors"></div>
                    </div>
                  </div>
                  </div>
          </fieldset>
					<fieldset>
					<legend class="navbar-brand">วันที่ปฏิบัติงาน</legend>
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 col-xl-2">
							<div class="form-group required" >
								<label for="title"  class="control-label">สัปดาห์ที่ :</label>
								<?php
										// build week menu
										echo '<select name="week_return"  class="form-control" >' . PHP_EOL;

									$week_travel=$regis[0]['week_travel'];
										echo '  <option value="'.$week_travel.'">' . $week_travel . '</option>' . PHP_EOL;
										for ($w=1; $w<=53; $w++) {
												echo '  <option value="' . $w . '">' . $w . '</option>' . PHP_EOL;
										}
										echo '</select>' . PHP_EOL;
										?>
						</div>
				</div>
		<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 col-xl-2">
			<div class="form-group required" >
				<label for="title"  class="control-label">วันที่ :</label>
				<?php

						// build days menu
						echo '<select name="day_travel"  class="form-control" >' . PHP_EOL;
						$day_travel=$regis[0]['day_travel'];
							echo '  <option value="'.$day_travel.'">' . $day_travel . '</option>' . PHP_EOL;
						for ($d=1; $d<=31; $d++) {
								echo '  <option value="' . $d . '">' . $d . '</option>' . PHP_EOL;
						}
						echo '</select>' . PHP_EOL;
						?>
		</div>
</div>
<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 col-xl-2">
	<div class="form-group required">
		<label for="title"  class="control-label">เดือน :</label>
		<select class="form-control" id="month_travel" name="month_travel"  title="เดือน" required>
							<option value="<?php echo $regis[0]['month_travel']; ?>">
								<?php
							 $monthtravel=$regis[0]['month_travel'];
							 echo $month[$monthtravel]; ?></option>
							 <option value="">--- โปรดเลือกเดือน ---</option>
							<option value="1">January</option>
							<option value="1">January</option>
							<option value="2">February</option>
							<option value="3">March</option>
							<option value="4">April</option>
							<option value="5">May</option>
							<option value="6">June</option>
							<option value="7">July</option>
							<option value="8">August</option>
							<option value="9">September</option>
							<option value="10">October</option>
							<option value="11">November</option>
							<option value="12">December</option>
							</select>
		<div class="help-block with-errors"></div>
</div>
</div>
<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 col-xl-2">
<div class="form-group required">
<label for="title"  class="control-label">ปี :</label>
	<?php
			// lowest year wanted
			$cutoff = 2000;
			// current year
			$now = date('Y');
			// build years menu
			echo '<select name="year_travel"  class="form-control" >' . PHP_EOL;
			$year_travel=$regis[0]['year_travel'];
				echo '  <option value="'.$year_travel.'">' . $year_travel . '</option>' . PHP_EOL;
			for ($y=$now; $y>=$cutoff; $y--) {

					echo '  <option value="' . $y . '">' . $y . '</option>' . PHP_EOL;
			}
			echo '</select>' . PHP_EOL;
			?>
<div class="help-block with-errors"></div>
</div>
</div>
					</div>
				</fieldset>
				<fieldset>
				<legend class="navbar-brand">วันที่ปฏิบัติงานเสร็จสิ้น</legend>
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 col-xl-2">
						<div class="form-group required" >
							<label for="title"  class="control-label">สัปดาห์ที่ :</label>
							<?php
									// build week menu
									echo '<select name="week_return"  class="form-control" >' . PHP_EOL;
									$week_return=$regis[0]['week_return'];
										echo '  <option value="'.$week_return.'">' . $week_return . '</option>' . PHP_EOL;
									for ($w=1; $w<=53; $w++) {
											echo '  <option value="' . $w . '">' . $w . '</option>' . PHP_EOL;
									}
									echo '</select>' . PHP_EOL;
									?>
					</div>
			</div>
	<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 col-xl-2">
		<div class="form-group required" >
			<label for="title"  class="control-label">วันที่ :</label>
			<?php

					// build days menu
					echo '<select name="day_return"  class="form-control" >' . PHP_EOL;
					$day_return=$regis[0]['day_return'];
						echo '  <option value="'.$day_return.'">' . $day_return . '</option>' . PHP_EOL;
					for ($d=1; $d<=31; $d++) {
							echo '  <option value="' . $d . '">' . $d . '</option>' . PHP_EOL;
					}
					echo '</select>' . PHP_EOL;
					?>
	</div>
</div>
<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 col-xl-2">
<div class="form-group required">
	<label for="title"  class="control-label">เดือน :</label>
	<select class="form-control" id="month_return" name="month_return"  title="เดือน" required>
		<option value="<?php echo $regis[0]['month_travel']; ?>">
			<?php
		 $monthreturn=$regis[0]['month_return'];
		 echo $month[$monthreturn]; ?></option>
		 <option value="">--- โปรดเลือกเดือน ---</option>
		<option value="1">	January</option>
		<option value="2">February</option>
		<option value="3">March</option>
		<option value="4">April</option>
		<option value="5">May</option>
		<option value="6">June</option>
		<option value="7">July</option>
		<option value="8">August</option>
		<option value="9">September</option>
		<option value="10">October</option>
		<option value="11">November</option>
		<option value="12">December</option>
						</select>
	<div class="help-block with-errors"></div>
</div>
</div>
<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 col-xl-2">
<div class="form-group required">
<label for="title"  class="control-label">ปี :</label>
<?php
		// lowest year wanted
		$cutoff = 2000;
		// current year
		$now = date('Y');
		// build years menu
		echo '<select name="year_return"  class="form-control" >' . PHP_EOL;
		$year_return=$regis[0]['year_return'];
			echo '  <option value="'.$year_return.'">' . $year_return . '</option>' . PHP_EOL;
		for ($y=$now; $y>=$cutoff; $y--) {

				echo '  <option value="' . $y . '">' . $y . '</option>' . PHP_EOL;
		}
		echo '</select>' . PHP_EOL;
		?>
<div class="help-block with-errors"></div>
</div>
</div>
				</div>
			</fieldset>
        <fieldset>
          <legend>อื่นๆ</legend>
          <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
            <div class="form-group">
            <input rows="5" cols="20" type="text" name="other" id="other" class="form-control" placeholder="อื่นๆ" data-error=""  value="<?php echo $regis[0]['other']; ?>">
            </div>
          </div>
        </div>
        </fieldset>
          <fieldset>
            <button type="submit" class="btn btn-danger">บันทึกข้อมูล</button>
            <span id="xhr-progress" style="display: none;"><img src="Assets/icons/ajax-loader.gif"> กำลังบันทึกข้อมูล โปรดรอ...</span>
					</fieldset>
				</form>
					</div>
			</div>
	</div>


</div>
</div>
</div>


<footer class="footer">
<div class="container-fluid">
<div class="copyright pull-right">
	&copy; <script>document.write(new Date().getFullYear())</script></i> by <a href="http://203.157.15.110/boe/">CEI</a>
</div>
</div>
</footer>

</div>
</div>

	    <!-- นำเข้า Javascript jQuery -->
	    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

	    <!-- Include all compiled plugins (below), or include individual files as needed -->
	    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>


		<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/js/select2.min.js"></script>

		<script>

				$(function(){

					$(".selectlo-single").select2();

					//ดึงข้อมูล province จากไฟล์ get_data.php
					$.ajax({
						url:"get_address_test.php",
						dataType: "json", //กำหนดให้มีรูปแบบเป็น Json
						data:{show_province:'show_province'}, //ส่งค่าตัวแปร show_province เพื่อดึงข้อมูล จังหวัด
						success:function(data){

							//วนลูปแสดงข้อมูล ที่ได้จาก ตัวแปร data
							$.each(data, function( index, value ) {
								//แทรก Elements ใน id province  ด้วยคำสั่ง append
								  $("#province").append("<option value='"+ value.id +"'> " + value.name + "</option>");
							});
						}
					});


					//แสดงข้อมูล อำเภอ  โดยใช้คำสั่ง change จะทำงานกรณีมีการเปลี่ยนแปลงที่ #province
					$("#province").change(function(){

						//กำหนดให้ ตัวแปร province มีค่าเท่ากับ ค่าของ #province ที่กำลังถูกเลือกในขณะนั้น
						var province_id = $(this).val();

						$.ajax({
							url:"get_address_test.php",
							dataType: "json",//กำหนดให้มีรูปแบบเป็น Json
							data:{province_id:province_id},//ส่งค่าตัวแปร province_id เพื่อดึงข้อมูล อำเภอ ที่มี province_id เท่ากับค่าที่ส่งไป
							success:function(data){

								//กำหนดให้ข้อมูลใน #amphur เป็นค่าว่าง
								$("#amphur").text("");

								//วนลูปแสดงข้อมูล ที่ได้จาก ตัวแปร data
								$.each(data, function( index, value ) {

									//แทรก Elements ข้อมูลที่ได้  ใน id amphur  ด้วยคำสั่ง append
									  $("#amphur").append("<option value='"+ value.id +"'> " + value.name + "</option>");
								});
							}
						});

					});

					$("#amphur").change(function(){

						var amphur_id = $(this).val();

						$.ajax({
							url:"get_address_test.php",
							dataType: "json",
							data:{amphur_id:amphur_id},
							success:function(data){

								  $("#district").text("");

								$.each(data, function( index, value ) {

								  $("#district").append("<option value='" + value.id + "'> " + value.name + "</option>");

								});
							}
						});

					});




				});

		</script>
			<script type="text/javascript">
			$(document).ready(function(){
			$("#job_etc").attr("disabled", true);
			$("#divis_etc").attr("disabled", true);
				$(".job").click(function() {
						$("#job_etc").attr("disabled", true);
						if ($("input[name=job]:checked").val() == "5") {
								$("#job_etc").attr("disabled", false);
						}
				});
				$(".divis").click(function() {
						$("#disvis_etc").attr("disabled", true);
						if ($("input[name=divis]:checked").val() == "6") {
								$("#divis_etc").attr("disabled", false);
						}
				});
				$(".finished-text").click(function(){
					var result = confirm("ต้องการส่งข้อมูล ?");
					if (result) {
							$("form[name='myForm']").submit();
					}else{
							location.reload();
					}
				});
			});
						</script>
						<script type="text/javascript">
						$(document).ready(function(){
			$(".addCF").click(function(){
				$("#customFields").append('<tr valign="top"><th scope="row"><label for="customFieldName">Custom Field</label></th><td><input type="text" class="code" id="customFieldName" name="customFieldName[]" value="" placeholder="Input Name" /> &nbsp; <input type="text" class="code" id="customFieldValue" name="customFieldValue[]" value="" placeholder="Input Value" /> &nbsp; <a href="javascript:void(0);" class="remCF">Remove</a></td></tr>');
			});
				$("#customFields").on('click','.remCF',function(){
						$(this).parent().parent().remove();
				});
		});
						</script>

						<script>
							var x = document.getElementById("coordinates");
								function getLocation() {
									if (navigator.geolocation) {
										navigator.geolocation.getCurrentPosition(showPosition);
									} else {
										x.innerHTML = "Geolocation is not supported by this browser.";
									}
								}
								function showPosition(position) {
									x.innerHTML = 'Latitude:<input type="text" name="lat" value="' + position.coords.latitude + '" class="form-control" />'+
										'Longitude:<input type="text" name="lon" value="' +position.coords.longitude + '" class="form-control" />';
									}
						</script>
						<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/css/select2.min.css" rel="stylesheet" />
						<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
					<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/js/select2.min.js"></script>
						<script>
						$(function(){

							$(".select2-single").select2();

							$.ajax({
								url:"get_disease.php",
								dataType: "json",
								data:{show_groupdisease:'show_groupdisease'},
								success:function(data){
									$.each(data, function( index, value ) {
											$("#groupdisease").append("<option value='"+ value.id +"'> " + value.name + "</option>");
									});
								}
							});
							$("#groupdisease").change(function(){
								var groupdisease_id = $(this).val();
								$.ajax({
									url:"get_disease.php",
									dataType: "json",
									data:{groupdisease_id:groupdisease_id},
									success:function(data){
										$("#disease").text("");
										$.each(data, function( index, value ) {
												$("#disease").append("<option value='"+ value.id +"'> " + value.name + "</option>");
										});
									}
								});
							});
						});
						</script>
						<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
						<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
						<script>
								$(document).ready(function(){
									var date_input=$('input[name="travel_date"]'); //our date input has the name "date"
									var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
									date_input.datepicker({
										format: 'yyyy-mm-dd',
										container: container,
										todayHighlight: true,
										autoclose: true,
									})
								})
						</script>
						<script>
								$(document).ready(function(){
									var date_input=$('input[name="return_date"]'); //our date input has the name "date"
									var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
									date_input.datepicker({
										format: 'yyyy-mm-dd',
										container: container,
										todayHighlight: true,
										autoclose: true,
									})
								})
					</script>
    </body>
    </html>
