<?php
 require_once 'include/querys.php';
 require_once 'include/valdisease.php';
 require_once 'include/vallocation.php';
 // require_once 'include/valdate.php';
 $query = new querys();


 // $sql = "SELECT * FROM tbl_groupdisease ORDER BY GROUPDISEASE_ID ASC";
 // $stmt = $dbh_db->prepare($sql);
 // $stmt->execute();
 // $ds_group=$stmt->fetchAll(PDO::FETCH_ASSOC);

 ?>
 <!doctype html>
 <html lang="en">
 <head>
   <meta charset="utf-8" />
   <link rel="apple-touch-icon" sizes="76x76" href="Assets/img/apple-icon.png">
   <link rel="icon" type="image/png" sizes="96x96" href="Assets/img/favicon.png">
   <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

   <title>Joint investigation team</title>

   <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
     <meta name="viewport" content="width=device-width" />

     <!--  Social tags      -->
     <meta name="keywords" content="bootstrap dashboard, creative tim, html dashboard, html css dashboard, web dashboard, paper design, bootstrap dashboard, bootstrap, css3 dashboard, bootstrap admin, paper bootstrap dashboard, frontend, responsive bootstrap dashboard">
     <meta name="description" content="Paper Dashboard is a beautiful Bootstrap Admin Panel for your next project.">
     <!-- Schema.org markup for Google+ -->
     <meta itemprop="name" content="Paper Dashboard by Creative Tim">
     <meta itemprop="description" content="Paper Dashboard is a beautiful Bootstrap Admin Panel for your next project.">
     <meta itemprop="image" content="http://s3.amazonaws.com/creativetim_bucket/products/43/original/opt_pd_thumbnail.jpg">
     <!-- Twitter Card data -->
     <meta name="twitter:card" content="summary_large_image">
     <meta name="twitter:site" content="@creativetim">
     <meta name="twitter:title" content="Paper Dashboard PRO by Creative Tim">
     <meta name="twitter:description" content="Paper Dashboard is a beautiful Bootstrap Admin Panel for your next project.">
     <meta name="twitter:creator" content="@creativetim">
     <meta name="twitter:image" content="http://s3.amazonaws.com/creativetim_bucket/products/43/original/opt_pd_thumbnail.jpg">
     <!-- Open Graph data -->
     <meta property="og:title" content="Paper Dashboard by Creative Tim" />
     <meta property="og:type" content="article" />
     <meta property="og:url" content="http://demos.creative-tim.com/paper-dashboard/dashboard.html" />
     <meta property="og:image" content="http://s3.amazonaws.com/creativetim_bucket/products/43/original/opt_pd_thumbnail.jpg"/>
     <meta property="og:description" content="Paper Dashboard is a beautiful Bootstrap Admin Panel for your next project." />
     <meta property="og:site_name" content="Creative Tim" />
     <!-- Bootstrap core CSS     -->
     <link href="Assets/css/bootstrap.min.css" rel="stylesheet" />
     <!-- Animation library for notifications   -->
     <link href="Assets/css/animate.min.css" rel="stylesheet"/>
     <!--  Paper Dashboard core CSS    -->
     <link href="Assets/css/paper-dashboard.css" rel="stylesheet"/>
     <!--  CSS for Demo Purpose, don't include it in your project     -->
     <!-- <link href="Assets/css/demo.css" rel="stylesheet" /> -->
     <!--  Fonts and icons     -->
     <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
     <link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>
     <link href="Assets/css/themify-icons.css" rel="stylesheet">
     <script src="Assets/jquery/jquery.min.js" type="text/javascript"></script>
         <!-- Paper Dashboard Core javascript and methods for Demo purpose -->
      <script src="Assets/js/paper-dashboard.js"></script>
      <link rel="stylesheet"href="https://google-developers.appspot.com/_static/0d76052693/css/devsite-cyan.css">
      <script src="https://google-developers.appspot.com/_static/0d76052693/js/prettify-bundle.js"></script>
      <script src="https://google-developers.appspot.com/_static/0d76052693/js/jquery-bundle.js"></script>
      <script src="//www.google.com/jsapi?key=AIzaSyCZfHRnq7tigC-COeQRmoa9Cxr0vbrK6xw"></script>
      <script src="https://google-developers.appspot.com/_static/0d76052693/js/framebox.js"></script>
     <script src="Assets/jquery/jquery.min.js" type="text/javascript"></script>
     <script src="Assets/bootstrap/js/moment.js" type="text/javascript"></script>
     <script src="Assets/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
     <script src="Assets/bootstrap/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
     <script src="Assets/jquery/jquery-ui.min.js" type="text/javascript"></script>
     <script src="Assets/bootstrap/js/validator.js" type="text/javascript"></script>

     <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
</head>
<body>

<div class="wrapper">
 <div class="sidebar" data-background-color="white" data-active-color="danger">

    <!--
   Tip 1: you can change the color of the sidebar's background using: data-background-color="white | black"
   Tip 2: you can change the color of the active button using the data-active-color="primary | info | success | warning | danger"
 -->

     <div class="sidebar-wrapper">
            <div class="logo">
                <a href="" class="simple-text">
                    JOINT INVESTIGATION TEAM
                </a>
            </div>
			<ul class="nav">
						 <li>
							 <a href="dashboard.php">
								 <i class="ti-panel"></i>
								 <p>Dashboard</p>
							 </a>
						 </li>
						 <li>
							 <a href="teamfrm.php">
								 <i class="ti-user"></i>
								 <p>กรอกข้อมูลทีมสอบสวนโรค</p>
							 </a>
						 </li>
				 <li class="active">
							 <a href="iaplst.php">
								 <i class="ti-marker"></i>
								 <p>รายงานสถานการณ์ประจำวัน</p>
							 </a>
						 </li>
						 <li>
							 <a href="http://www.boeeoc.moph.go.th/eventbase/calendar/zone99/">
								 <i class="ti-notepad"></i>
								 <p>ปฏิทินแจ้งข่าวการระบาด</p>
							 </a>
						 </li>
						 <li>
							 <a href="teamlst.php">
								 <i class="ti-view-list-alt"></i>
								 <p>ตารางทีมที่ออกสอบสวน</p>
							 </a>
						 </li>
						 <li>
							 <a href="http://203.157.15.110/boe/software/downloadtab.php#tab5">
								 <i class="ti-clipboard"></i>
								 <p>แบบฟอร์มสอบสวนโรค</p>
							 </a>
						 </li>
						 <li>
							 <a href="maps.php">
								 <i class="ti-map"></i>
								 <p>Maps</p>
							 </a>
						 </li>
				<li class="active-pro">
						 </li>
					 </ul>
     </div>
    </div>

    <div class="main-panel">
   <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar bar1"></span>
                        <span class="icon-bar bar2"></span>
                        <span class="icon-bar bar3"></span>
                    </button>
                    <a class="navbar-brand" href="#">ข้อมูลโรคและสถานที่ออกสอบสวนโรค</a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="ti-panel"></i>
               <p>Stats</p>
                            </a>
                        </li>
                        <li class="dropdown">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="ti-bell"></i>
                                    <p class="notification">5</p>
                 <p>Notifications</p>
                 <b class="caret"></b>
                              </a>
                              <ul class="dropdown-menu">
                                <li><a href="#">Notification 1</a></li>
                                <li><a href="#">Notification 2</a></li>
                                <li><a href="#">Notification 3</a></li>
                                <li><a href="#">Notification 4</a></li>
                                <li><a href="#">Another notification</a></li>
                              </ul>
                        </li>
           <li>
             <a href="logout.php">
                 <i class="ti-power-off"></i>
               <p>logout</p>
                            </a>
                        </li>
                    </ul>

                </div>
            </div>
        </nav>
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">ตารางการรายงานสถานการณ์ประจำวัน</h4>
                            </div>
                            <div class="content table-responsive table-full-width">
                              <table id="example" class="hover" cellspacing="0" width="100%">
                                <thead>
                                  <tr>
									<th>Event Id</th>
                                    <!-- <th>รายชื่อสมาชิกทีม</th> -->
                                    <th>สถานที่</th>
                                    <th>วันที่ออกสอบสวน</th>
                                    <th>วันที่กลับ</th>
                                    <th>กลุ่มโรค-โรค</th>
                                    <th>ดูข้อมูล / แก้ไขข้อมูล</th>
                                  </tr>
                                </thead>
                                <tfoot></tfoot>
                                <tbody>
                                <?php
                                $regis = $query->getRegister();
                                foreach ($regis as $k=>$v) {
                                  echo "<tr>\n";
									echo "<td>".$v['event_id']."</td>\n";
                                    // echo "<td>"."<button type=\"button\" class=\"btn btn-info btn-sm\" data-toggle=\"modal\" data-target=\"#myModal\">รายชื่อสมาชิกทึม</i></button>\n";
                                    // echo "<td>"."<a href=\"personlst.php?Id_Team=".$v['Id_Team']."\" title=\"ดูข้อมูล\" class=\"btn btn-xs btn-info\"><i class=\"fa fa-eye\" aria-hidden=\"true\"></i></a>\n";
                                    echo "<td>".$arr_district[$v['district_name']]."".$arr_amphur[$v['amphur_name']]." ".$arr_province[$v['province_name']]."</td>\n";
                                    echo "<td>".$v['day_travel']." ".$v['month_travel']." ".$v['year_travel']."</td>\n";
                                    echo "<td>".$v['day_return']." ".$v['month_return']." ".$v['year_return']."</td>\n";
                                    echo "<td>".$groupdisease[$v['groupdisease_name']]." "," ".$disease[$v['disease_name']]." "," ".$v['disease_other_name']."</td>\n";
                                    echo "<td>\n";
                                    echo "<a href=\"jitteamview.php?Id_Team=".$v['Id_Team']."\" title=\"ดูข้อมูล\" class=\"btn btn-xs btn-info\"><i class=\"fa fa-eye\" aria-hidden=\"true\"></i></a>\n";
                                    	echo "<a href=\"iapcaselst.php?event_id=".$v['event_id']."&Id_Team=".$v['Id_Team']."\" title=\"เพิ่มสถานการณ์ประจำวัน\" class=\"btn btn-xs btn-warning\"><i class=\"fa fa-pencil\" aria-hidden=\"true\"></i></a>\n";
                                    echo "</td>\n";
                                  echo "</tr>\n";
                                }
                                ?>
                          <!-- Modal -->
                          <div class="modal fade" id="myModal" role="dialog">
                            <div class="modal-dialog">
                              <!-- Modal content-->
                              <div class="modal-content">
                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                  <h4 class="modal-title">สมาชิกทีม</h4>
                                </div>
                                <div class="modal-body">
                                  <fieldset>
                                    <legend>รหัสทีมสอบสวน</legend>
                                    <div class="row">
                                      <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                        <div class="form-group required">
                                          <label for="title" class="control-label">รหัสทีมสอบสวน :</label>
                                          <input type="text" name="Id_Team" id="Id_Team" class="form-control" placeholder="รหัสทีมสอบสวน" data-error="" value="<?php echo $v['Id_Team']; ?>" required>
                                          <div class="help-block with-errors"></div>
                                        </div>
                                      </div>
                                  </div>
                                  </fieldset>
                                </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                              </div>

                            </div>
                          </div>
                                </tbody>
                              </table>

                            </div>
                        </div>
                    </div>

                    <footer class="footer">
                        <div class="container-fluid">
            				<div class="copyright pull-right">
                                &copy; <script>document.write(new Date().getFullYear())</script></i> by <a href="http://203.157.15.110/boe/">CEI</a>
                            </div>
                        </div>
                    </footer>

    </div>
</div>

    </div>
</div>

</body>
<script src="Assets/js/jquery-1.10.2.js" type="text/javascript"></script>
<script src="Assets/js/bootstrap.min.js" type="text/javascript"></script>
<script>
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>
    <!--   Core JS Files   -->
    <script src="https://code.jquery.com/jquery-1.12.4.js" type="text/javascript"></script>
	<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js" type="text/javascript"></script>

	<!--  Checkbox, Radio & Switch Plugins -->
	<script src="Assets/js/bootstrap-checkbox-radio.js"></script>

	<!--  Charts Plugin -->
	<script src="Assets/js/chartist.min.js"></script>

    <!--  Notifications Plugin    -->
    <script src="Assets/js/bootstrap-notify.js"></script>

    <!--  Google Maps Plugin    -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script>

    <!-- Paper Dashboard Core javascript and methods for Demo purpose -->
	<script src="Assets/js/paper-dashboard.js"></script>

	<!-- Paper Dashboard DEMO methods, don't include it in your project! -->
	<script src="Assets/js/demo.js"></script>
    <script src="Assets/js/jquery.sharrre.js"></script>

</html>
